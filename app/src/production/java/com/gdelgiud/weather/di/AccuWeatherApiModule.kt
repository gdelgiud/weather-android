package com.gdelgiud.weather.di

import com.gdelgiud.weather.BuildConfig
import com.gdelgiud.weather.current.resource.AccuWeatherCurrentWeatherApi
import com.gdelgiud.weather.forecast.daily.resource.AccuWeatherDailyForecastApi
import com.gdelgiud.weather.forecast.hourly.resource.AccuWeatherHourlyForecastApi
import com.gdelgiud.weather.search.resource.AccuWeatherSearchApi
import com.gdelgiud.weather.utils.networking.ApiServiceFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class AccuWeatherApiModule {

    @Provides
    fun provideCurrentWeatherApi(factory: ApiServiceFactory): AccuWeatherCurrentWeatherApi =
        factory.create(AccuWeatherCurrentWeatherApi::class.java)

    @Provides
    fun provideHourlyForecastApi(factory: ApiServiceFactory): AccuWeatherHourlyForecastApi =
        factory.create(AccuWeatherHourlyForecastApi::class.java)

    @Provides
    fun provideDailyForecastApi(factory: ApiServiceFactory): AccuWeatherDailyForecastApi =
        factory.create(AccuWeatherDailyForecastApi::class.java)

    @Provides
    fun provideSearchApi(factory: ApiServiceFactory): AccuWeatherSearchApi =
        factory.create(AccuWeatherSearchApi::class.java)

    @Provides
    @ApiKey
    fun provideApiKey() = BuildConfig.ACCUWEATHER_API_KEY

}
