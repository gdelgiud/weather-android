package com.gdelgiud.weather.forecast.hourly.resource

import android.content.Context
import com.gdelgiud.weather.R
import com.gdelgiud.weather.forecast.hourly.resource.response.HourlyForecastResponse
import com.gdelgiud.weather.utils.RawFileReader
import com.gdelgiud.weather.utils.time.HeaderProvider
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import okhttp3.Headers.Companion.headersOf
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import kotlin.random.Random

class MockAccuWeatherHourlyForecastApi @Inject constructor(
    @ApplicationContext private val context: Context,
    private val headerProvider: HeaderProvider,
    private val json: Json
) : AccuWeatherHourlyForecastApi {

    override suspend fun get12HourForecast(
        locationId: String,
        apiKey: String,
        language: String
    ): Response<List<HourlyForecastResponse>> {
        return withContext(Dispatchers.IO) {
            val raw = RawFileReader.read(context, R.raw.forecast_12_hours_mock)
            val forecast = json.decodeFromString<List<HourlyForecastResponse>>(raw)
            delay(Random.nextLong(from = 500, until = 1000))
            Timber.d("AccuWeather mock: Hourly Forecast")
            Timber.d(json.decodeFromString<JsonElement>(raw).toString())
            return@withContext Response.success(
                forecast,
                headersOf(*headerProvider.getExpiresHeader())
            )
        }
    }

}
