package com.gdelgiud.weather.forecast.daily.resource

import android.content.Context
import com.gdelgiud.weather.R
import com.gdelgiud.weather.forecast.daily.resource.response.DailyForecastResponse
import com.gdelgiud.weather.utils.RawFileReader
import com.gdelgiud.weather.utils.time.HeaderProvider
import com.gdelgiud.weather.utils.time.TimeProvider
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import okhttp3.Headers.Companion.headersOf
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import kotlin.random.Random

class MockAccuWeatherDailyForecastApi @Inject constructor(
    @ApplicationContext private val context: Context,
    private val headerProvider: HeaderProvider,
    private val timeProvider: TimeProvider,
    private val json: Json
) : AccuWeatherDailyForecastApi {

    override suspend fun get5DayForecast(
        locationId: String,
        apiKey: String,
        language: String
    ): Response<DailyForecastResponse> {
        return withContext(Dispatchers.IO) {
            val raw = RawFileReader.read(context, R.raw.forecast_5_days_mock)
            val forecast = json.decodeFromString<DailyForecastResponse>(raw).withTweakedDates()
            delay(Random.nextLong(from = 500, until = 1000))
            Timber.d("AccuWeather mock: Daily Forecast")
            Timber.d(json.decodeFromString<JsonElement>(raw).toString())
            return@withContext toResponse(forecast)
        }
    }

    private fun DailyForecastResponse.withTweakedDates() =
        copy(
            DailyForecasts = DailyForecasts.mapIndexed { i, day ->
                day.copy(
                    Date = timeProvider.now().plusDays(i.toLong())
                )
            }
        )

    private fun toResponse(body: DailyForecastResponse): Response<DailyForecastResponse> {
        return Response.success(body, headersOf(*headerProvider.getExpiresHeader()))
    }

}
