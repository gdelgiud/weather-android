package com.gdelgiud.weather.current.resource

import android.content.Context
import com.gdelgiud.weather.R
import com.gdelgiud.weather.current.resource.response.CurrentWeatherResponse
import com.gdelgiud.weather.utils.RawFileReader
import com.gdelgiud.weather.utils.time.HeaderProvider
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import okhttp3.Headers
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import kotlin.random.Random

class MockAccuWeatherCurrentWeatherApi @Inject constructor(
    @ApplicationContext private val context: Context,
    private val headerProvider: HeaderProvider,
    private val json: Json
) : AccuWeatherCurrentWeatherApi {

    override suspend fun getCurrentWeather(
        locationId: String,
        apiKey: String,
        language: String
    ): Response<List<CurrentWeatherResponse>> =
        withContext(Dispatchers.IO) {
            val raw = RawFileReader.read(context, R.raw.current_weather_mock)
            val currentWeather = json.decodeFromString(raw) as List<CurrentWeatherResponse>
            delay(Random.nextLong(from = 500, until = 1000))
            Timber.d("AccuWeather mock: Current Weather")
            Timber.d(json.decodeFromString<JsonElement>(raw).toString())
            return@withContext toResponse(currentWeather)
        }

    private fun toResponse(body: List<CurrentWeatherResponse>): Response<List<CurrentWeatherResponse>> {
        return Response.success(body, Headers.headersOf(*headerProvider.getExpiresHeader()))
    }

}
