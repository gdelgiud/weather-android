package com.gdelgiud.weather.di

import com.gdelgiud.weather.current.resource.AccuWeatherCurrentWeatherApi
import com.gdelgiud.weather.current.resource.MockAccuWeatherCurrentWeatherApi
import com.gdelgiud.weather.forecast.daily.resource.AccuWeatherDailyForecastApi
import com.gdelgiud.weather.forecast.daily.resource.MockAccuWeatherDailyForecastApi
import com.gdelgiud.weather.forecast.hourly.resource.AccuWeatherHourlyForecastApi
import com.gdelgiud.weather.forecast.hourly.resource.MockAccuWeatherHourlyForecastApi
import com.gdelgiud.weather.search.resource.AccuWeatherSearchApi
import com.gdelgiud.weather.search.resource.MockAccuWeatherSearchLocationApi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface AccuWeatherApiModule {

    @Binds
    fun provideCurrentWeatherApi(api: MockAccuWeatherCurrentWeatherApi): AccuWeatherCurrentWeatherApi

    @Binds
    fun provideHourlyForecastApi(api: MockAccuWeatherHourlyForecastApi): AccuWeatherHourlyForecastApi

    @Binds
    fun provideDailyForecastApi(api: MockAccuWeatherDailyForecastApi): AccuWeatherDailyForecastApi

    @Binds
    fun provideSearchApi(api: MockAccuWeatherSearchLocationApi): AccuWeatherSearchApi

    companion object {

        @Provides
        @ApiKey
        fun provideApiKey() = "Mock API Key"

    }
}
