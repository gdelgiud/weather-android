package com.gdelgiud.weather.utils.time

import dagger.Reusable
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@Reusable
class HeaderProvider @Inject constructor(
    private val timeProvider: TimeProvider
) {

    fun getExpiresHeader() =
        arrayOf("Expires", DateTimeFormatter.RFC_1123_DATE_TIME.format(getExpirationDate()))

    private fun getExpirationDate() = timeProvider.now().plusMinutes(10)

}
