package com.gdelgiud.weather.utils

import android.content.Context
import androidx.annotation.RawRes
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class RawFileReader {

    companion object {

        fun read(context: Context, @RawRes resId: Int): String {
            val inputStream: InputStream = context.resources.openRawResource(resId)
            val inputreader = InputStreamReader(inputStream)
            val buffreader = BufferedReader(inputreader)
            var line: String?
            val text = StringBuilder()
            try {
                while (buffreader.readLine().also { line = it } != null) {
                    text.append(line)
                    text.append('\n')
                }
            } catch (e: IOException) {
                throw IllegalArgumentException("Could not find raw resource with ID: $resId")
            }
            return text.toString()
        }
    }
}
