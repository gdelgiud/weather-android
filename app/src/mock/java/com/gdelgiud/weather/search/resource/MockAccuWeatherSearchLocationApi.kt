package com.gdelgiud.weather.search.resource

import android.content.Context
import com.gdelgiud.weather.R
import com.gdelgiud.weather.search.resource.response.LocationSearchResponse
import com.gdelgiud.weather.utils.RawFileReader
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import timber.log.Timber
import javax.inject.Inject
import kotlin.random.Random

class MockAccuWeatherSearchLocationApi @Inject constructor(
    @ApplicationContext private val context: Context,
    private val json: Json
) : AccuWeatherSearchApi {

    override suspend fun search(
        term: String,
        apiKey: String,
        language: String
    ): List<LocationSearchResponse> {
        return withContext(Dispatchers.IO) {
            val raw = RawFileReader.read(context, R.raw.search_locations_mock)
            val results = json.decodeFromString<List<LocationSearchResponse>>(raw)
            delay(Random.nextLong(from = 500, until = 1000))
            Timber.d("AccuWeather mock: Search Location")
            Timber.d(json.decodeFromString<JsonElement>(raw).toString())
            return@withContext results.filter { item -> item.LocalizedName.contains(term, true) }
        }
    }

}
