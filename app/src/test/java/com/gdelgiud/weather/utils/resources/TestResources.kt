package com.gdelgiud.weather.utils.resources

import android.content.res.Resources
import androidx.annotation.StringRes
import java.util.Locale

@Suppress("DEPRECATION")
class TestResources : Resources(null, null, null) {

    private val strings = HashMap<Int, String>()

    fun putString(@StringRes id: Int, string: String) {
        strings[id] = string
    }

    override fun getString(id: Int): String {
        return strings.getOrDefault(id, "")
    }

    override fun getString(id: Int, vararg formatArgs: Any?): String {
        return String.format(Locale.US, getString(id), *formatArgs)
    }
}
