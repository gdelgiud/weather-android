package com.gdelgiud.weather.utils.networking

import com.gdelgiud.weather.utils.serialization.JsonFactory
import okhttp3.mockwebserver.MockWebServer

fun <T> MockWebServer.createTestService(type: Class<T>): T =
    ApiServiceFactory(url("/"), JsonFactory()).create(type)
