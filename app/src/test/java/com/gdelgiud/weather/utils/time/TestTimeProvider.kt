package com.gdelgiud.weather.utils.time

import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

class TestTimeProvider : TimeProvider {

    private val date = OffsetDateTime.of(LocalDate.of(2011, 11, 10), LocalTime.NOON, ZoneOffset.UTC)

    override fun now(): OffsetDateTime = date

    fun expiredDate() = date.minus(100, ChronoUnit.MILLIS)

    fun nonExpiredDate() = date.plus(100, ChronoUnit.MILLIS)

}
