package com.gdelgiud.weather.utils.locale

import java.util.Locale

class TestLocaleProvider(private val locale: Locale = Locale.US) : LocaleProvider {

    override fun getLanguageTag(): String = locale.toLanguageTag()

    override fun getLocale() = locale

}
