package com.gdelgiud.weather.search.resource

import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.utils.locale.TestLocaleProvider
import com.gdelgiud.weather.utils.networking.MockResponseFileReader
import com.gdelgiud.weather.utils.networking.createTestService
import com.gdelgiud.weather.utils.time.TestTimeProvider
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.HttpURLConnection
import java.util.Locale

internal class AccuWeatherSearchLocationServiceTest {

    private val mockWebServer = MockWebServer()

    private val api = mockWebServer.createTestService(AccuWeatherSearchApi::class.java)
    private val timeProvider = TestTimeProvider()
    private val service = AccuWeatherSearchLocationService(
        api,
        TestLocaleProvider(Locale.US),
        timeProvider,
        "apiKey"
    )

    @Test
    fun `given successful response then emits mapped current weather`() = runTest {
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        val actualLocationResult = service.search("search term")

        val expectedLocationResult = LocationResult(
            id = "7894",
            name = "Buenos Aires",
            countryName = "Argentina",
            creationTime = timeProvider.now(),
            LocationResult.Source.RESULT
        )
        assertEquals(listOf(expectedLocationResult), actualLocationResult)
    }

    @Test
    fun `when invoke then calls api with correct parameters`() = runTest {
        val searchTerm = "search term"
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        service.search(searchTerm)

        val request = mockWebServer.takeRequest()
        val requestUrl = request.requestUrl!!
        assertEquals("GET", request.method, "Method")
        assertEquals("apiKey", requestUrl.queryParameter("apikey"), "API Key")
        assertEquals("en-US", requestUrl.queryParameter("language"), "Language")
        assertEquals(searchTerm, requestUrl.queryParameter("q"), "Search term")
        assertEquals("/locations/v1/cities/autocomplete", requestUrl.encodedPath, "URL Path")
    }

    @AfterEach
    fun afterEach() {
        mockWebServer.shutdown()
    }

    private fun getSuccessfulMockResponse() = MockResponse()
        .setResponseCode(HttpURLConnection.HTTP_OK)
        .setBody(MockResponseFileReader("search/success.json").content)
}
