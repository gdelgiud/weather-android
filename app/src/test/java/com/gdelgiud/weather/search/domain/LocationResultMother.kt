package com.gdelgiud.weather.search.domain

import java.time.OffsetDateTime

object LocationResultMother {

    fun anyLocationResult() =
        LocationResult(
            "locationId",
            "cityName",
            "countryName",
            OffsetDateTime.now(),
            LocationResult.Source.HISTORY
        )

    fun locationWithId(selectedLocationId: String) =
        anyLocationResult().copy(id = selectedLocationId)
}
