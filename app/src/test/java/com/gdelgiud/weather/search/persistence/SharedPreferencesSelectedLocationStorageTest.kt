package com.gdelgiud.weather.search.persistence

import app.cash.turbine.test
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.storage.TestSharedPreferences
import com.gdelgiud.weather.utils.time.TestTimeProvider
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SharedPreferencesSelectedLocationStorageTest {

    private val sharedPreferences = TestSharedPreferences()
    private val timeProvider = TestTimeProvider()

    private val storage = SharedPreferencesSelectedLocationStorage(sharedPreferences, timeProvider)

    @Test
    fun `given shared preferences is empty when storage is created then does not emit`() = runTest {
        sharedPreferences.edit().clear().apply()

        storage.get().test {
            // No items expected
        }
    }

    @Test
    fun `given shared preferences has value when storage is created then emits stored value`() = runTest {
        val storedLocation = anyLocationResult()
        storage.store(storedLocation)

        storage.get().test {
            assertEquals(storedLocation, awaitItem())
        }
    }

    @Test
    fun `given stored value when value changes then emits new value`() = runTest {
        val originalLocation = anyLocationResult()
        val newLocation = originalLocation.copy(id = "id2")

        storage.store(originalLocation)
        storage.store(newLocation)

        storage.get().test {
            assertEquals(newLocation, awaitItem())
        }
    }

    private fun anyLocationResult() =
        LocationResult(
            "id",
            "Buenos Aires",
            "Argentina",
            timeProvider.now(),
            LocationResult.Source.RESULT
        )

}
