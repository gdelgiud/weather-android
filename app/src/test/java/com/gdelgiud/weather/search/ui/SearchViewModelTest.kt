package com.gdelgiud.weather.search.ui

import androidx.lifecycle.viewModelScope
import app.cash.turbine.test
import com.gdelgiud.weather.R
import com.gdelgiud.weather.search.domain.LocationHistoryStorage
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.search.domain.LocationResultMother.anyLocationResult
import com.gdelgiud.weather.search.domain.SearchLocationService
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.search.ui.SearchViewModel.Companion.SEARCH_DELAY_TIME_MILLIS
import com.gdelgiud.weather.search.ui.model.Idle
import com.gdelgiud.weather.search.ui.model.Loading
import com.gdelgiud.weather.search.ui.model.LocationResultUiModel
import com.gdelgiud.weather.search.ui.model.Results
import com.gdelgiud.weather.utils.coroutines.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset

@ExtendWith(CoroutinesTestExtension::class)
internal class SearchViewModelTest {

    private val searchLocationService = mockk<SearchLocationService>()

    private val selectedLocationStorage = mockk<SelectedLocationStorage> {
        coEvery { store(any()) } returns Unit
    }
    private val locationHistoryStorage = mockk<LocationHistoryStorage> {
        coEvery { store(any()) } returns Unit
        coEvery { getHistory() } returns emptyList()
    }

    private val viewModel: SearchViewModel by lazy {
        SearchViewModel(
            searchLocationService,
            selectedLocationStorage,
            locationHistoryStorage
        )
    }

    @Test
    fun `given history items when load then emits history items as result`() = runTest {
        val locationHistoryItem = anyLocationResult()
        coEvery { locationHistoryStorage.getHistory() } returns listOf(locationHistoryItem)

        val expectedResult = LocationResultUiModel(
            locationHistoryItem.id,
            locationHistoryItem.name,
            locationHistoryItem.countryName,
            R.drawable.ic_history
        )
        viewModel.uiState.test {
            assertEquals(Results(listOf(expectedResult), ""), awaitItem())
        }
    }

    @Test
    fun `given history items when load then emits history items sorted by creation date`() = runTest {
        val now = OffsetDateTime.of(LocalDate.of(2000, 1, 1), LocalTime.MIDNIGHT, ZoneOffset.UTC)
        val item1 = anyLocationResult().copy(id = "1", creationTime = now)
        val item2 = anyLocationResult().copy(id = "2", creationTime = now.plusHours(1))
        val item3 = anyLocationResult().copy(id = "3", creationTime = now.plusHours(2))
        val item4 = anyLocationResult().copy(id = "4", creationTime = now.plusHours(3))
        coEvery { locationHistoryStorage.getHistory() } returns listOf(item2, item1, item3, item4)

        val expectedResult = listOf("4", "3", "2", "1")
        viewModel.uiState.test {
            assertEquals(expectedResult, (awaitItem() as Results).results.map(LocationResultUiModel::id))
        }
    }

    @Test
    fun `given no history items when load then emits idle state`() = runTest {
        coEvery { locationHistoryStorage.getHistory() } returns emptyList()

        viewModel.uiState.test {
            assertEquals(Idle(""), awaitItem())
        }
    }

    @Test
    fun `given no previous results when search text is changed then status changes to loading`() = runTest {
        val searchText = "a city name"
        viewModel.onSearchTextChanged(searchText)

        viewModel.uiState.test {
            assertEquals(Loading(searchText), awaitItem())
        }
    }

    @Test
    fun `when search text is changed then search is not performed immediately`() {
        viewModel.onSearchTextChanged("a city name")

        coVerify(exactly = 0) { searchLocationService.search(any()) }
    }

    @Test
    fun `when search text is changed then search is performed after debounce time`() = runTest {
        val term = "a city name"

        viewModel.onSearchTextChanged(term)
        advanceTimeBy(SEARCH_DELAY_TIME_MILLIS + 10)

        coVerify { searchLocationService.search(term) }
    }

    @Test
    fun `when search text is changed twice in a row then search is performed once`() = runTest {
        val firstTerm = "first search"
        val secondTerm = "second search"

        viewModel.onSearchTextChanged(firstTerm)
        advanceTimeBy(SEARCH_DELAY_TIME_MILLIS / 2)
        viewModel.onSearchTextChanged(secondTerm)
        advanceTimeBy(SEARCH_DELAY_TIME_MILLIS + 10)

        coVerify(exactly = 1) { searchLocationService.search(secondTerm) }
    }

    @Test
    fun `given search results when received then emits result state`() = runTest {
        val resultItem = anyLocationResult().copy(source = LocationResult.Source.RESULT)
        val searchText = "a city name"
        givenPreviousSearchResults(resultItem, searchText)

        val expectedResult = LocationResultUiModel(
            resultItem.id,
            resultItem.name,
            resultItem.countryName,
            R.drawable.ic_place
        )
        viewModel.uiState.test {
            assertEquals(Results(listOf(expectedResult), searchText), awaitItem())
        }
    }

    @Test
    fun `given history items when item is clicked then sets location as currently selected`() {
        val historyItem = anyLocationResult()
        coEvery { locationHistoryStorage.getHistory() } returns listOf(historyItem)

        viewModel.onResultItemClicked(historyItem.id)

        coVerify { selectedLocationStorage.store(historyItem) }
    }

    @Test
    fun `given history items when item is clicked then stores item in history`() {
        val historyItem = anyLocationResult()
        coEvery { locationHistoryStorage.getHistory() } returns listOf(historyItem)

        viewModel.onResultItemClicked(historyItem.id)

        coVerify { locationHistoryStorage.store(historyItem) }
    }

    @Test
    fun `given history items when item with unknown id is clicked then does not set item as currently selected`() {
        coEvery { locationHistoryStorage.getHistory() } returns listOf(anyLocationResult())

        viewModel.onResultItemClicked("unknown")

        coVerify(exactly = 0) { selectedLocationStorage.store(any()) }
    }

    @Test
    fun `given history items when item with unknown id is clicked then does not store item in history`() {
        coEvery { locationHistoryStorage.getHistory() } returns listOf(anyLocationResult())

        viewModel.onResultItemClicked("unknown")

        coVerify(exactly = 0) { locationHistoryStorage.store(any()) }
    }

    @Test
    fun `given previous search results when item is clicked then sets location as currently selected`() = runTest {
        val searchResult = anyLocationResult()
        givenPreviousSearchResults(searchResult, "a city name")

        viewModel.onResultItemClicked(searchResult.id)

        coVerify { selectedLocationStorage.store(searchResult) }
    }

    @Test
    fun `given previous search results when item is clicked then stores item in history`() = runTest {
        val searchResult = anyLocationResult()
        givenPreviousSearchResults(searchResult, "a city name")

        viewModel.onResultItemClicked(searchResult.id)

        coVerify { locationHistoryStorage.store(searchResult) }
    }

    @Test
    fun `given previous search results when item with unknown id is clicked then does not set location as currently selected`() = runTest {
        givenPreviousSearchResults(anyLocationResult(), "a city name")

        viewModel.onResultItemClicked("unknown")

        coVerify(exactly = 0) { selectedLocationStorage.store(any()) }
    }

    @Test
    fun `given previous search results when item with unknown id is clicked then does not store item in history`() = runTest {
        givenPreviousSearchResults(anyLocationResult(), "a city name")

        viewModel.onResultItemClicked("unknown")

        coVerify(exactly = 0) { locationHistoryStorage.store(any()) }
    }

    @AfterEach
    fun tearDown() {
        viewModel.viewModelScope.cancel()
    }

    private fun TestScope.givenPreviousSearchResults(searchResult: LocationResult, searchText: String) {
        coEvery { searchLocationService.search(searchText) } returns listOf(searchResult)
        viewModel.onSearchTextChanged(searchText)
        advanceTimeBy(SEARCH_DELAY_TIME_MILLIS + 10)
    }
}
