package com.gdelgiud.weather.storage

import android.content.SharedPreferences

class TestSharedPreferences : SharedPreferences {

    private val values = mutableMapOf<String?, Any?>()

    override fun getAll() = values

    override fun getString(key: String?, defaultValue: String?) = get(key, defaultValue)

    override fun getStringSet(key: String?, defaultValue: MutableSet<String>?) =
        get(key, defaultValue)

    override fun getInt(key: String?, defaultValue: Int) = get(key, defaultValue)

    override fun getLong(key: String?, defaultValue: Long) = get(key, defaultValue)

    override fun getFloat(key: String?, defaultValue: Float) = get(key, defaultValue)

    override fun getBoolean(key: String?, defaultValue: Boolean) = get(key, defaultValue)

    @Suppress("UNCHECKED_CAST")
    private fun <T> get(key: String?, defaultValue: T) = values[key] as T ?: defaultValue

    override fun contains(key: String?) = values.containsKey(key)

    override fun edit(): SharedPreferences.Editor = EditorImpl()

    override fun registerOnSharedPreferenceChangeListener(key: SharedPreferences.OnSharedPreferenceChangeListener?) {
        TODO("Not yet implemented")
    }

    override fun unregisterOnSharedPreferenceChangeListener(key: SharedPreferences.OnSharedPreferenceChangeListener?) {
        TODO("Not yet implemented")
    }

    inner class EditorImpl : SharedPreferences.Editor {

        private val editedValues = HashMap<String?, Any?>()
        private val valuesToRemove = mutableSetOf<String?>()
        private var shouldClear = false

        override fun putString(key: String?, newValue: String?): SharedPreferences.Editor {
            editedValues[key] = newValue
            return this
        }

        override fun putStringSet(
            key: String?,
            newValue: MutableSet<String>?
        ): SharedPreferences.Editor {
            editedValues[key] = newValue
            return this
        }

        override fun putInt(key: String?, newValue: Int): SharedPreferences.Editor {
            editedValues[key] = newValue
            return this
        }

        override fun putLong(key: String?, newValue: Long): SharedPreferences.Editor {
            editedValues[key] = newValue
            return this
        }

        override fun putFloat(key: String?, newValue: Float): SharedPreferences.Editor {
            editedValues[key] = newValue
            return this
        }

        override fun putBoolean(key: String?, newValue: Boolean): SharedPreferences.Editor {
            editedValues[key] = newValue
            return this
        }

        override fun remove(key: String?): SharedPreferences.Editor {
            valuesToRemove.add(key)
            return this
        }

        override fun clear(): SharedPreferences.Editor {
            shouldClear = true
            return this
        }

        override fun commit(): Boolean {
            apply()
            return true
        }

        override fun apply() {
            if (shouldClear) {
                this@TestSharedPreferences.values.clear()
                shouldClear = false
            }

            editedValues.forEach { (key, value) ->
                if (value != null) {
                    this@TestSharedPreferences.values[key] = value
                } else {
                    this@TestSharedPreferences.values.remove(key)
                }
            }
            valuesToRemove.forEach { key ->
                this@TestSharedPreferences.values.remove(key)
            }

            editedValues.clear()
            valuesToRemove.clear()
        }

    }

}
