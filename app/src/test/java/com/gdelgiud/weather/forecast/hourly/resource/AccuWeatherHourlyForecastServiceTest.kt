package com.gdelgiud.weather.forecast.hourly.resource

import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastItem
import com.gdelgiud.weather.utils.locale.TestLocaleProvider
import com.gdelgiud.weather.utils.networking.MockResponseFileReader
import com.gdelgiud.weather.utils.networking.createTestService
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.HttpURLConnection
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.Locale

internal class AccuWeatherHourlyForecastServiceTest {

    private val mockWebServer = MockWebServer()

    private val api = mockWebServer.createTestService(AccuWeatherHourlyForecastApi::class.java)
    private val service = AccuWeatherHourlyForecastService(
        api,
        TestLocaleProvider(Locale.US),
        "apiKey"
    )

    @Test
    fun `given successful response then emits mapped current weather`() = runTest {
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        val result = service.get12HoursForecast("locationId")

        val expectedHourlyForecastItem = HourlyForecastItem(
            date = OffsetDateTime.of(2020, 2, 15, 15, 0, 0, 0, ZoneOffset.ofHours(-3)),
            weatherStatus = "Sunny",
            precipitationProbability = 13f,
            weatherIcon = 1,
            temperature = 28.3f
        )
        assertEquals(HourlyForecast(listOf(expectedHourlyForecastItem)), result)
    }

    @Test
    fun `given successful response with expiration date then emits current weather with expiration date`() = runTest {
        val receivedDate = "Thu, 30 Aug 2012 14:56:34 GMT"
        val expectedDate = OffsetDateTime.of(2012, 8, 30, 14, 56, 34, 0, ZoneOffset.UTC)
        val response = getSuccessfulMockResponse().setHeader("Expires", receivedDate)
        mockWebServer.enqueue(response)

        val result = service.get12HoursForecast("locationId")

        assertEquals(expectedDate, result.expirationDate)
    }

    @Test
    fun `when invoke then calls api with correct parameters`() = runTest {
        val locationId = "locationId"
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        service.get12HoursForecast(locationId)

        val request = mockWebServer.takeRequest()
        val requestUrl = request.requestUrl!!
        assertEquals("GET", request.method, "Method")
        assertEquals("apiKey", requestUrl.queryParameter("apikey"), "API Key")
        assertEquals("en-US", requestUrl.queryParameter("language"), "Language")
        assertEquals("/forecasts/v1/hourly/12hour/${locationId}", requestUrl.encodedPath, "URL Path")
    }

    @AfterEach
    fun afterEach() {
        mockWebServer.shutdown()
    }

    private fun getSuccessfulMockResponse() = MockResponse()
        .setResponseCode(HttpURLConnection.HTTP_OK)
        .setBody(MockResponseFileReader("hourly_forecast/success.json").content)
}
