package com.gdelgiud.weather.forecast.daily.resource

import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DayForecast
import com.gdelgiud.weather.forecast.daily.domain.Schedule
import com.gdelgiud.weather.utils.locale.TestLocaleProvider
import com.gdelgiud.weather.utils.networking.MockResponseFileReader
import com.gdelgiud.weather.utils.networking.createTestService
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.HttpURLConnection
import java.time.OffsetDateTime
import java.time.ZoneOffset

internal class AccuWeatherDailyForecastServiceTest {

    private val mockWebServer = MockWebServer()

    private val api = mockWebServer.createTestService(AccuWeatherDailyForecastApi::class.java)
    private val service = AccuWeatherDailyForecastService(api, TestLocaleProvider(), "apiKey")

    @Test
    fun `given successful response then emits mapped daily forecast`() = runTest {
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        val result = service.get5DayForecast("locationId")

        val expectedSchedule = Schedule(
            sunrise = OffsetDateTime.of(2020, 4, 18, 6, 0, 0, 0, ZoneOffset.ofHours(-3)),
            sunset = OffsetDateTime.of(2020, 4, 18, 20, 0, 0, 0, ZoneOffset.ofHours(-3))
        )
        val expectedDailyForecastItem = DayForecast(
            date = OffsetDateTime.of(2020, 4, 18, 7, 0, 0, 0, ZoneOffset.ofHours(-3)),
            minimumTemperature = 16.7f,
            maximumTemperature = 22.8f,
            schedule = expectedSchedule,
            weatherIcon = 1
        )
        val expectedDailyForecast = DailyForecast(
            days = listOf(expectedDailyForecastItem),
            description = "Pleasant this weekend"
        )
        assertEquals(expectedDailyForecast, result)
    }

    @Test
    fun `given successful response with expiration date then emits daily forecast with expiration date`() = runTest {
        val receivedDate = "Thu, 30 Aug 2012 14:56:34 GMT"
        val expectedDate = OffsetDateTime.of(2012, 8, 30, 14, 56, 34, 0, ZoneOffset.UTC)
        val response = getSuccessfulMockResponse().setHeader("Expires", receivedDate)
        mockWebServer.enqueue(response)

        val result = service.get5DayForecast("locationId")

        assertEquals(expectedDate, result.expirationDate)
    }

    @Test
    fun `when invoke then calls api with correct parameters`() = runTest {
        val locationId = "locationId"
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        service.get5DayForecast(locationId)

        val request = mockWebServer.takeRequest()
        val requestUrl = request.requestUrl!!
        assertEquals("GET", request.method, "Method")
        assertEquals("apiKey", requestUrl.queryParameter("apikey"), "API Key")
        assertEquals("en-US", requestUrl.queryParameter("language"), "Language")
        assertEquals("/forecasts/v1/daily/5day/${locationId}", requestUrl.encodedPath, "URL Path")
    }

    @AfterEach
    fun afterEach() {
        mockWebServer.shutdown()
    }

    private fun getSuccessfulMockResponse() = MockResponse()
        .setResponseCode(HttpURLConnection.HTTP_OK)
        .setBody(MockResponseFileReader("daily_forecast/success.json").content)

}
