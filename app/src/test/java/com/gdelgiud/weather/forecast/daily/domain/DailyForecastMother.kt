package com.gdelgiud.weather.forecast.daily.domain

import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset

object DailyForecastMother {

    fun anyDailyForecast() =
        DailyForecast(
            days = listOf(anyDailyForecastItem()),
            description = "Pleasant this weekend",
            expirationDate = null
        )

    fun anyDailyForecastItem() = DayForecast(OffsetDateTime.now(), 12f, 30f, anySchedule(), 4)

    fun dailyForecastItemWithSunriseAt(sunrise: OffsetDateTime) =
        anyDailyForecastItem().copy(
            date = sunrise,
            schedule = anySchedule().copy(sunrise = sunrise)
        )

    fun dailyForecastItemWithSunsetAt(sunset: OffsetDateTime) =
        anyDailyForecastItem().copy(
            date = sunset,
            schedule = anySchedule().copy(sunset = sunset)
        )

    fun anySchedule() = Schedule(
        sunrise = OffsetDateTime.of(LocalDate.of(2020, 11, 12), LocalTime.of(7, 0), ZoneOffset.UTC),
        sunset = OffsetDateTime.of(LocalDate.of(2020, 11, 12), LocalTime.of(8, 0), ZoneOffset.UTC)
    )

    fun dailyForecastWithItems(vararg items: DayForecast) =
        anyDailyForecast().copy(days = items.asList())

    fun dailyForecastWithExpirationDate(expirationDate: OffsetDateTime?) =
        anyDailyForecast().copy(expirationDate = expirationDate)
}
