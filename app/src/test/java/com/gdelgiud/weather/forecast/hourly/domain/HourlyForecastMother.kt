package com.gdelgiud.weather.forecast.hourly.domain

import java.time.OffsetDateTime

object HourlyForecastMother {

    fun anyHourlyForecast() =
        HourlyForecast(
            hours = listOf(anyHourlyForecastItem()),
            expirationDate = null
        )

    fun anyHourlyForecastItem() = HourlyForecastItem(OffsetDateTime.now(), "Sunny", 3, 10f, 12f)

    fun hourlyForecastWithExpirationDate(expirationDate: OffsetDateTime?) =
        anyHourlyForecast().copy(expirationDate = expirationDate)
}
