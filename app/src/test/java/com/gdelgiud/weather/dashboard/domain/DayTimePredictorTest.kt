package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.current.domain.DayTime
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.dailyForecastItemWithSunriseAt
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.dailyForecastItemWithSunsetAt
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.dailyForecastWithItems
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastStorage
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.search.domain.LocationResultMother.anyLocationResult
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.utils.coroutines.CoroutinesTestExtension
import com.gdelgiud.weather.utils.time.TestTimeProvider
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(CoroutinesTestExtension::class)
internal class DayTimePredictorTest {

    private val dailyForecastStorage = mockk<DailyForecastStorage> {
        coEvery { remove(any()) } returns Unit
    }
    private val selectedLocationStorage = mockk<SelectedLocationStorage>()
    private val timeProvider = TestTimeProvider()

    private val dayTimePredictor = DayTimePredictor(
        dailyForecastStorage,
        selectedLocationStorage,
        timeProvider
    )

    @Test
    fun `given no stored location then returns null`() = runTest {
        givenNoSelectedLocation()

        val result = dayTimePredictor.getDayTime()

        assertNull(result)
    }

    @Test
    fun `given selected location and no cached forecast then returns null`() = runTest {
        val selectedLocation = anyLocationResult()
        givenSelectedLocation(selectedLocation)
        coEvery { dailyForecastStorage.get(selectedLocation.id) } returns null

        val result = dayTimePredictor.getDayTime()

        assertNull(result)
    }

    @Test
    fun givenMatchingDayAndCurrentTimeIsBeforeSunriseThenReturnsNightTime() = runTest {
        val selectedLocation = anyLocationResult()
        val todaysForecast = dailyForecastItemWithSunriseAt(timeProvider.now().plusHours(1))
        val forecast = dailyForecastWithItems(todaysForecast)
        givenSelectedLocation(selectedLocation)
        coEvery { dailyForecastStorage.get(selectedLocation.id) } returns forecast

        val result = dayTimePredictor.getDayTime()

        assertEquals(DayTime.NIGHT, result)
    }

    @Test
    fun givenMatchingDayAndCurrentTimeIsAfterSunriseAndBeforeSunsetThenReturnsDayTime() = runTest {
        val selectedLocation = anyLocationResult()
        val todaysForecast = dailyForecastItemWithSunriseAt(timeProvider.now().minusHours(1))
        val forecast = dailyForecastWithItems(todaysForecast)
        givenSelectedLocation(selectedLocation)
        coEvery { dailyForecastStorage.get(selectedLocation.id) } returns forecast

        val result = dayTimePredictor.getDayTime()

        assertEquals(DayTime.DAY, result)
    }

    @Test
    fun givenMatchingDayAndCurrentTimeIsAfterSunsetThenReturnsNightTime() = runTest {
        val selectedLocation = anyLocationResult()
        val todaysForecast = dailyForecastItemWithSunsetAt(timeProvider.now().minusHours(1))
        val forecast = dailyForecastWithItems(todaysForecast)
        givenSelectedLocation(selectedLocation)
        coEvery { dailyForecastStorage.get(selectedLocation.id) } returns forecast

        val result = dayTimePredictor.getDayTime()

        assertEquals(DayTime.NIGHT, result)
    }

    @Test
    fun givenForecastStorageFailureThenReturnsNull() = runTest {
        val selectedLocation = anyLocationResult()
        givenSelectedLocation(selectedLocation)
        coEvery { dailyForecastStorage.get(selectedLocation.id) } throws RuntimeException()

        val result = dayTimePredictor.getDayTime()

        assertNull(result)
    }

    @Test
    fun givenForecastStorageFailureThenRemovesCachedForecast() = runTest {
        val selectedLocation = anyLocationResult()
        givenSelectedLocation(selectedLocation)
        coEvery { dailyForecastStorage.get(selectedLocation.id) } throws RuntimeException()

        dayTimePredictor.getDayTime()

        coVerify { dailyForecastStorage.remove(selectedLocation.id) }
    }

    private fun givenNoSelectedLocation() {
        every { selectedLocationStorage.isPresent() } returns false
    }

    private fun givenSelectedLocation(selectedLocation: LocationResult) {
        every { selectedLocationStorage.isPresent() } returns true
        every { selectedLocationStorage.get() } returns flowOf(selectedLocation)
    }
}
