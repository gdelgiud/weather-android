package com.gdelgiud.weather.dashboard.ui

import androidx.lifecycle.viewModelScope
import app.cash.turbine.test
import com.gdelgiud.weather.R
import com.gdelgiud.weather.current.domain.DayTime
import com.gdelgiud.weather.dashboard.domain.DashboardMother.anyDashboard
import com.gdelgiud.weather.dashboard.domain.DayTimePredictor
import com.gdelgiud.weather.dashboard.domain.LoadDashboardService
import com.gdelgiud.weather.dashboard.ui.model.CurrentWeatherUiModel
import com.gdelgiud.weather.dashboard.ui.model.DashboardUiModel
import com.gdelgiud.weather.dashboard.ui.model.Error
import com.gdelgiud.weather.dashboard.ui.model.Loading
import com.gdelgiud.weather.dashboard.ui.model.Success
import com.gdelgiud.weather.search.domain.LocationResultMother.anyLocationResult
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.utils.coroutines.CoroutinesTestExtension
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertInstanceOf
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(CoroutinesTestExtension::class)
internal class DashboardViewModelTest {

    private val selectedLocationStorage = mockk<SelectedLocationStorage> {
        every { getLatest() } returns anyLocationResult()
    }
    private val loadDashboardService = mockk<LoadDashboardService> {
        coEvery { loadedDashboard } returns flowOf()
    }
    private val dashboardInfoToUiModelMapper = mockk<DashboardInfoToUiModelMapper>()
    private val dayTimePredictor = mockk<DayTimePredictor> {
        every { getDayTime() } returns DayTime.DAY
    }

    private val viewModel: DashboardViewModel by lazy {
        DashboardViewModel(
            selectedLocationStorage,
            loadDashboardService,
            dashboardInfoToUiModelMapper,
            dayTimePredictor
        )
    }

    @Test
    fun `given no predicted day time when start then emits loading with default color`() = runTest {
        val selectedLocation = anyLocationResult()
        every { selectedLocationStorage.getLatest() } returns selectedLocation
        coEvery { dayTimePredictor.getDayTime() } returns null

        viewModel.onStart()

        val expectedState = Loading(
            cityName = selectedLocation.name,
            countryName = selectedLocation.countryName,
            dayTime = null
        )
        viewModel.dashboardData.test {
            assertEquals(expectedState, expectMostRecentItem())
        }
    }

    @Test
    fun `given day time when start then emits loading with day time color`() = runTest {
        val selectedLocation = anyLocationResult()
        every { selectedLocationStorage.getLatest() } returns selectedLocation
        coEvery { dayTimePredictor.getDayTime() } returns DayTime.DAY

        viewModel.onStart()

        val expectedState = Loading(
            cityName = selectedLocation.name,
            countryName = selectedLocation.countryName,
            dayTime = DayTime.DAY
        )
        viewModel.dashboardData.test {
            assertEquals(expectedState, expectMostRecentItem())
        }
    }

    @Test
    fun `given night time when start then emits loading with night time color`() = runTest {
        val selectedLocation = anyLocationResult()
        every { selectedLocationStorage.getLatest() } returns selectedLocation
        coEvery { dayTimePredictor.getDayTime() } returns DayTime.NIGHT

        viewModel.onStart()

        val expectedState = Loading(
            cityName = selectedLocation.name,
            countryName = selectedLocation.countryName,
            dayTime = DayTime.NIGHT
        )
        viewModel.dashboardData.test {
            assertEquals(expectedState, expectMostRecentItem())
        }
    }

    @Test
    fun `given successful dashboard info retrieval when start then emits mapped ui model`() = runTest {
        val dashboardInfo = anyDashboard()
        val expectedDashboardUiModel = DashboardUiModel(
            cityName = "Buenos Aires",
            countryName = "Argentina",
            currentWeather = CurrentWeatherUiModel(
                currentTemperature = "25",
                weatherStatus = "Mostly sunny",
                weatherIconRes = R.drawable.weather_icon_mostly_sunny
            ),
            hourlyForecast = listOf(),
            dailyForecast = listOf(),
            dailyForecastDescription = "Pleasant this weekend",
            dayTime = DayTime.DAY
        )
        coEvery { loadDashboardService.loadedDashboard } returns flowOf(dashboardInfo)
        every { dashboardInfoToUiModelMapper.map(dashboardInfo) } returns expectedDashboardUiModel

        viewModel.onStart()

        viewModel.dashboardData.test {
            assertEquals(Success(
                cityName = expectedDashboardUiModel.cityName,
                countryName = expectedDashboardUiModel.countryName,
                currentWeather = expectedDashboardUiModel.currentWeather,
                hourlyForecast = expectedDashboardUiModel.hourlyForecast,
                dailyForecast = expectedDashboardUiModel.dailyForecast,
                dailyForecastDescription = expectedDashboardUiModel.dailyForecastDescription,
                dayTime = expectedDashboardUiModel.dayTime
            ), awaitItem())
        }
    }

    @Test
    fun `given dashboard info retrieval failure when start then emits error`() = runTest {
        coEvery { loadDashboardService.loadedDashboard } returns flow { error("dashboard error") }

        viewModel.onStart()

        viewModel.dashboardData.test {
            assertInstanceOf(Error::class.java, awaitItem())
        }
    }

    @Test
    fun `given dashboard info retrieval failure when retry button is clicked then retries dashboard info retrieval`() = runTest {
        coEvery { loadDashboardService.loadedDashboard } returns flow { error("dashboard error") }
        clearMocks(loadDashboardService, answers = false)
        viewModel.onStart()

        viewModel.onRetryClicked()

        coVerify { loadDashboardService.loadedDashboard }
    }

    @Test
    fun `given no previous failure when retry button is clicked then does not retrieve dashboard info`() = runTest {
        viewModel.onRetryClicked()

        coVerify(exactly = 0) { loadDashboardService.loadedDashboard }
    }

    @Test
    fun `given successful dashboard info retrieval when retry button is clicked then does not retrieve dashboard info again`() = runTest {
        coEvery { loadDashboardService.loadedDashboard } returns flowOf(anyDashboard())
        every { dashboardInfoToUiModelMapper.map(any()) } returns mockk()
        clearMocks(loadDashboardService)

        viewModel.onRetryClicked()

        coVerify(exactly = 0) { loadDashboardService.loadedDashboard }
    }

    @AfterEach
    fun tearDown() {
        viewModel.viewModelScope.cancel()
    }

}
