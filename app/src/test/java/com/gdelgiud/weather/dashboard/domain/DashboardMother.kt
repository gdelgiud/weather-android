package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.current.domain.CurrentWeatherMother
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastMother
import com.gdelgiud.weather.search.domain.LocationResultMother
import java.time.OffsetDateTime

object DashboardMother {

    fun anyDashboard() =
        DashboardInfo(
            currentWeather = CurrentWeatherMother.anyCurrentWeather(),
            hourlyForecast = HourlyForecastMother.anyHourlyForecast().hours,
            dailyForecast = DailyForecastMother.anyDailyForecast(),
            selectedLocation = LocationResultMother.anyLocationResult()
        )

    fun dashboardWithExpirationDate(expirationDate: OffsetDateTime?) =
        anyDashboard().copy(expirationDate = expirationDate)
}
