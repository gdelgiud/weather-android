package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.current.domain.CurrentWeatherMother.anyCurrentWeather
import com.gdelgiud.weather.current.domain.CurrentWeatherMother.currentWeatherWithExpirationDate
import com.gdelgiud.weather.current.domain.CurrentWeatherService
import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.anyDailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.dailyForecastWithExpirationDate
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastService
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastStorage
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastMother.anyHourlyForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastMother.hourlyForecastWithExpirationDate
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastService
import com.gdelgiud.weather.search.domain.LocationResultMother.anyLocationResult
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset

internal class DefaultRemoteDashboardServiceTest {

    private val currentWeatherService = mockk<CurrentWeatherService>()
    private val hourlyForecastService = mockk<HourlyForecastService>()
    private val dailyForecastService = mockk<DailyForecastService>()
    private val dailyForecastStorage = mockk<DailyForecastStorage> {
        coEvery { store(any(), any()) } returns Unit
    }

    private val service = DefaultRemoteDashboardService(
        currentWeatherService,
        hourlyForecastService,
        dailyForecastService,
        dailyForecastStorage
    )

    @Test
    fun `given selected location then collects info from all services`() = runTest {
        val selectedLocation = anyLocationResult()
        val currentWeather = anyCurrentWeather()
        val hourlyForecast = anyHourlyForecast()
        val dailyForecast = anyDailyForecast()
        givenCurrentWeather(selectedLocation.id, currentWeather)
        givenHourlyForecast(selectedLocation.id, hourlyForecast)
        givenDailyForecast(selectedLocation.id, dailyForecast)

        val result = service.get(selectedLocation)

        val expectedInfo = DashboardInfo(
            currentWeather,
            hourlyForecast.hours,
            dailyForecast,
            selectedLocation
        )
        assertEquals(expectedInfo, result)
    }

    @Test
    fun `given current weather failure then emits same failure`() = runTest {
        val exception = RuntimeException("current weather error")
        val selectedLocation = anyLocationResult()
        givenHourlyForecast(selectedLocation.id, anyHourlyForecast())
        givenDailyForecast(selectedLocation.id, anyDailyForecast())
        coEvery { currentWeatherService.getCurrentWeather(any()) } throws exception

        assertThrows<RuntimeException>("current weather error") { service.get(selectedLocation) }
    }

    @Test
    fun `given selected location and elements with expiration date then emits dashboard with earliest expiration date`() = runTest {
        val selectedLocation = anyLocationResult()
        val earliestDate = OffsetDateTime.ofInstant(Instant.ofEpochMilli(123543L), ZoneOffset.UTC)
        givenCurrentWeather(selectedLocation.id, currentWeatherWithExpirationDate(earliestDate))
        givenHourlyForecast(
            selectedLocation.id,
            hourlyForecastWithExpirationDate(earliestDate.plusSeconds(1))
        )
        givenDailyForecast(
            selectedLocation.id,
            dailyForecastWithExpirationDate(earliestDate.plusSeconds(3))
        )

        val result = service.get(selectedLocation)

        assertEquals(earliestDate, result.expirationDate)
    }

    @Test
    fun `given selected location and elements without expiration date then emits dashboard with no expiration date`() = runTest {
        val selectedLocation = anyLocationResult()
        givenCurrentWeather(selectedLocation.id, currentWeatherWithExpirationDate(null))
        givenHourlyForecast(selectedLocation.id, hourlyForecastWithExpirationDate(null))
        givenDailyForecast(selectedLocation.id, dailyForecastWithExpirationDate(null))

        val result = service.get(selectedLocation)

        assertNull(result.expirationDate)
    }

    @Test
    fun `given successful daily forecast response then caches result`() = runTest {
        val selectedLocation = anyLocationResult()
        val dailyForecast = anyDailyForecast()
        givenCurrentWeather(selectedLocation.id, anyCurrentWeather())
        givenHourlyForecast(selectedLocation.id, anyHourlyForecast())
        givenDailyForecast(selectedLocation.id, dailyForecast)

        service.get(selectedLocation)

        coVerify { dailyForecastStorage.store(selectedLocation.id, dailyForecast) }
    }

    private fun givenCurrentWeather(selectedLocationId: String, currentWeather: CurrentWeather) {
        coEvery { currentWeatherService.getCurrentWeather(selectedLocationId) } returns currentWeather
    }

    private fun givenHourlyForecast(selectedLocationId: String, hourlyForecast: HourlyForecast) {
        coEvery { hourlyForecastService.get12HoursForecast(selectedLocationId) } returns hourlyForecast
    }

    private fun givenDailyForecast(selectedLocationId: String, dailyForecast: DailyForecast) {
        coEvery { dailyForecastService.get5DayForecast(selectedLocationId) } returns dailyForecast
    }
}
