package com.gdelgiud.weather.dashboard.persistence.memory

import app.cash.turbine.test
import com.gdelgiud.weather.dashboard.domain.DashboardMother.anyDashboard
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime

internal class InMemoryLocalDashboardInfoStorageTest {

    private val storage = InMemoryLocalDashboardInfoStorage()

    @Test
    fun `given no stored dashboard when get then emits empty`() = runTest {
        storage.get("id").test {
            assertEquals(null, awaitItem())
        }
    }

    @Test
    fun `given stored dashboard when get then emits dashboard`() = runTest {
        val dashboardInfo = anyDashboard()
        storage.store("id", dashboardInfo)

        storage.get("id").test {
            assertEquals(dashboardInfo, awaitItem())
        }
    }

    @Test
    fun `given stored dashboard when dashboard gets updated then emits new dashboard`() = runTest {
        val originalDashboard = anyDashboard()
        val newDashboard = originalDashboard.copy(expirationDate = OffsetDateTime.now())
        storage.store("id", originalDashboard)
        storage.store("id", newDashboard)

        storage.get("id").test {
            assertEquals(newDashboard, awaitItem())
        }
    }

    @Test
    fun `given stored dashboard when clear then emits null`() = runTest {
        storage.store("id", anyDashboard())
        storage.remove("id")

        storage.get("id").test {
            assertEquals(null, awaitItem())
        }
    }

}
