package com.gdelgiud.weather.dashboard.domain

import app.cash.turbine.test
import com.gdelgiud.weather.dashboard.domain.DashboardMother.anyDashboard
import com.gdelgiud.weather.dashboard.domain.DashboardMother.dashboardWithExpirationDate
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.search.domain.LocationResultMother.locationWithId
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.utils.time.TestTimeProvider
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.Test

internal class LoadDashboardServiceTest {

    private val selectedLocationId = "selectedLocationId"

    private val timeProvider = TestTimeProvider()
    private val localStorage = mockk<LocalDashboardInfoStorage> {
        coEvery { store(any(), any()) } returns Unit
        coEvery { remove(any()) } returns Unit
    }
    private val remoteService = mockk<RemoteDashboardService>()
    private val selectedLocationStorage = mockk<SelectedLocationStorage>()
    private val service = LoadDashboardService(
        localStorage,
        remoteService,
        selectedLocationStorage,
        timeProvider
    )

    @Test
    fun `given non expired local dashboard then emits local dashboard`() = runTest {
        val cachedDashboard = dashboardWithExpirationDate(timeProvider.nonExpiredDate())
        givenSelectedLocation(locationWithId(selectedLocationId))
        givenCachedDashboard(cachedDashboard)

        service.loadedDashboard.test {
            assertEquals(cachedDashboard, awaitItem())
        }
    }

    @Test
    fun `given non expired local dashboard then does not invoke remote service`() = runTest {
        val selectedLocation = locationWithId(selectedLocationId)
        givenSelectedLocation(selectedLocation)
        givenCachedDashboard(dashboardWithExpirationDate(timeProvider.nonExpiredDate()))

        service.loadedDashboard

        coVerify(exactly = 0) { remoteService.get(selectedLocation) }
    }

    @Test
    fun `given no local dashboard then invokes remote service`() = runTest {
        val selectedLocation = locationWithId(selectedLocationId)
        val remoteDashboard = anyDashboard()
        givenSelectedLocation(selectedLocation)
        givenNoCachedDashboard()
        givenRemoteDashboard(remoteDashboard, selectedLocation)

        service.loadedDashboard.test {
            assertEquals(remoteDashboard, awaitItem())
        }
    }

    @Test
    fun `given expired local dashboard then invokes remote service`() = runTest {
        val selectedLocation = locationWithId(selectedLocationId)
        val expirationDate = timeProvider.expiredDate()
        val cachedDashboard = dashboardWithExpirationDate(expirationDate)
        val remoteDashboard = anyDashboard()
        givenSelectedLocation(selectedLocation)
        givenCachedDashboard(cachedDashboard)
        givenRemoteDashboard(remoteDashboard, selectedLocation)

        service.loadedDashboard.test {
            assertSame(remoteDashboard, awaitItem())
        }
    }

    @Test
    fun `given local dashboard without expiration date then invokes remote service`() = runTest {
        val selectedLocation = locationWithId(selectedLocationId)
        val remoteDashboard = anyDashboard()
        val cachedDashboard = dashboardWithExpirationDate(null)
        givenSelectedLocation(selectedLocation)
        givenCachedDashboard(cachedDashboard)
        givenRemoteDashboard(remoteDashboard, selectedLocation)

        service.loadedDashboard.test {
            assertSame(remoteDashboard, awaitItem())
        }
    }

    @Test
    fun `given successful remote dashboard then stores result in local storage`() = runTest {
        val selectedLocation = locationWithId(selectedLocationId)
        val remoteDashboard = anyDashboard()
        givenSelectedLocation(selectedLocation)
        givenNoCachedDashboard()
        givenRemoteDashboard(remoteDashboard, selectedLocation)

        service.loadedDashboard.test {
            cancelAndIgnoreRemainingEvents()
        }

        coVerify { localStorage.store(selectedLocationId, remoteDashboard) }
    }

    private fun givenSelectedLocation(selectedLocation: LocationResult) {
        coEvery { selectedLocationStorage.get() } returns MutableStateFlow(selectedLocation)
    }

    private fun givenCachedDashboard(cachedDashboard: DashboardInfo) {
        coEvery { localStorage.get(selectedLocationId) } returns flowOf(cachedDashboard)
    }

    private fun givenNoCachedDashboard() {
        coEvery { localStorage.get(selectedLocationId) } returns flowOf(null)
    }

    private fun givenRemoteDashboard(dashboard: DashboardInfo, selectedLocation: LocationResult) {
        coEvery { remoteService.get(selectedLocation) } returns dashboard
    }

}
