package com.gdelgiud.weather.dashboard.ui

import com.gdelgiud.weather.R
import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.current.domain.CurrentWeatherMother.anyCurrentWeather
import com.gdelgiud.weather.current.domain.CurrentWeatherMother.currentWeatherWithDaytime
import com.gdelgiud.weather.current.domain.CurrentWeatherMother.currentWeatherWithNighttime
import com.gdelgiud.weather.current.domain.DayTime
import com.gdelgiud.weather.dashboard.domain.DashboardInfo
import com.gdelgiud.weather.dashboard.ui.model.CurrentWeatherUiModel
import com.gdelgiud.weather.dashboard.ui.model.DailyForecastUiModel
import com.gdelgiud.weather.dashboard.ui.model.HourlyForecastUiModel
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.anyDailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.anySchedule
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastMother.dailyForecastWithItems
import com.gdelgiud.weather.forecast.daily.domain.DayForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastItem
import com.gdelgiud.weather.search.domain.LocationResultMother.anyLocationResult
import com.gdelgiud.weather.utils.locale.TestLocaleProvider
import com.gdelgiud.weather.utils.resources.TestResources
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset

internal class DashboardInfoToUiModelMapperTest {

    private val testResources = TestResources()
    private val localeProvider = TestLocaleProvider()
    private val stringProvider = DashboardStringProvider(testResources, localeProvider)
    private val mapper = DashboardInfoToUiModelMapper(stringProvider, localeProvider)

    @BeforeEach
    fun beforeEach() {
        testResources.putString(R.string.temperature, "%s°C")
        testResources.putString(R.string.precipitation_probability, "%s%%")
    }

    @Test
    fun `given dashboard info then maps city and country name`() {
        val selectedLocation = anyLocationResult()
        val inputDashboardInfo = DashboardInfo(
            currentWeather = anyCurrentWeather(),
            hourlyForecast = emptyList(),
            dailyForecast = anyDailyForecast(),
            selectedLocation = selectedLocation
        )

        val result = mapper.map(inputDashboardInfo)

        assertEquals(selectedLocation.name, result.cityName)
        assertEquals(selectedLocation.countryName, result.countryName)
    }

    @Test
    fun `given dashboard info then maps current weather`() {
        val selectedLocation = anyLocationResult()
        val inputDashboardInfo = DashboardInfo(
            currentWeather = CurrentWeather(10.542f, "Sunny", 1, true),
            hourlyForecast = emptyList(),
            dailyForecast = anyDailyForecast(),
            selectedLocation = selectedLocation
        )

        val result = mapper.map(inputDashboardInfo)

        val expectedCurrentWeather = CurrentWeatherUiModel(
            currentTemperature = "10.5°C",
            weatherStatus = inputDashboardInfo.currentWeather.weatherStatus,
            weatherIconRes = R.drawable.weather_icon_sunny
        )
        assertEquals(expectedCurrentWeather, result.currentWeather)
    }

    @Test
    fun `given dashboard info then maps hourly forecast`() {
        val time = LocalTime.of(23, 40)
        val date = OffsetDateTime.of(LocalDate.of(2000, 12, 13), time, ZoneOffset.UTC)
        val hourlyForecastElement = HourlyForecastItem(date, "Sunny", 1, 30.312f, 12.312f)
        val inputDashboardInfo = DashboardInfo(
            currentWeather = anyCurrentWeather(),
            hourlyForecast = listOf(hourlyForecastElement),
            dailyForecast = anyDailyForecast(),
            selectedLocation = anyLocationResult()
        )

        val result = mapper.map(inputDashboardInfo)

        val expectedHourlyForecastElement = HourlyForecastUiModel(
            temperature = "12.3°C",
            hourText = "23:40",
            weatherStatus = hourlyForecastElement.weatherStatus,
            precipitationProbabilityText = "30%",
            weatherIconRes = R.drawable.weather_icon_sunny
        )
        assertEquals(listOf(expectedHourlyForecastElement), result.hourlyForecast)
    }

    @Test
    fun `given dashboard info then maps daily forecast`() {
        val date = OffsetDateTime.of(LocalDate.of(2000, 12, 13), LocalTime.NOON, ZoneOffset.UTC)
        val dailyForecastElement = DayForecast(date, 12.412f, 25.125f, anySchedule(), 1)
        val inputDashboardInfo = DashboardInfo(
            currentWeather = anyCurrentWeather(),
            hourlyForecast = emptyList(),
            dailyForecast = dailyForecastWithItems(dailyForecastElement),
            selectedLocation = anyLocationResult()
        )

        val result = mapper.map(inputDashboardInfo)

        val expectedDailyForecastElement = DailyForecastUiModel(
            dayName = "Wed",
            minimumTemperature = "12°C",
            maximumTemperature = "25°C",
            weatherIconRes = R.drawable.weather_icon_sunny
        )
        assertEquals(listOf(expectedDailyForecastElement), result.dailyForecast)
    }

    @Test
    fun `given dashboard info with day time location then maps dashboard with day time background`() {
        val inputDashboardInfo = DashboardInfo(
            currentWeather = currentWeatherWithDaytime(),
            hourlyForecast = emptyList(),
            dailyForecast = anyDailyForecast(),
            selectedLocation = anyLocationResult()
        )

        val result = mapper.map(inputDashboardInfo)

        assertEquals(DayTime.DAY, result.dayTime)
    }

    @Test
    fun `given dashboard info with night time location then maps dashboard with night time background`() {
        val inputDashboardInfo = DashboardInfo(
            currentWeather = currentWeatherWithNighttime(),
            hourlyForecast = emptyList(),
            dailyForecast = anyDailyForecast(),
            selectedLocation = anyLocationResult()
        )

        val result = mapper.map(inputDashboardInfo)

        assertEquals(DayTime.NIGHT, result.dayTime)
    }

}
