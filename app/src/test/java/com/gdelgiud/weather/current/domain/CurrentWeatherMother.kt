package com.gdelgiud.weather.current.domain

import java.time.OffsetDateTime

object CurrentWeatherMother {

    fun anyCurrentWeather() = CurrentWeather(
        currentTemperature = 13f,
        weatherStatus = "Sunny",
        weatherIcon = 3,
        isDay = true,
        expirationDate = null
    )

    fun currentWeatherWithStatus(status: String) =
        anyCurrentWeather().copy(weatherStatus = status)

    fun currentWeatherWithExpirationDate(expirationDate: OffsetDateTime?) =
        anyCurrentWeather().copy(expirationDate = expirationDate)

    fun currentWeatherWithDaytime() =
        anyCurrentWeather().copy(isDay = true)

    fun currentWeatherWithNighttime() =
        anyCurrentWeather().copy(isDay = false)
}
