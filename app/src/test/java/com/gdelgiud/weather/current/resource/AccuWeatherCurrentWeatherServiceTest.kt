package com.gdelgiud.weather.current.resource

import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.utils.locale.TestLocaleProvider
import com.gdelgiud.weather.utils.networking.MockResponseFileReader
import com.gdelgiud.weather.utils.networking.createTestService
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.HttpURLConnection
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.Locale

internal class AccuWeatherCurrentWeatherServiceTest {

    private val mockWebServer = MockWebServer()

    private val api = mockWebServer.createTestService(AccuWeatherCurrentWeatherApi::class.java)
    private val service = AccuWeatherCurrentWeatherService(
        api,
        TestLocaleProvider(Locale.US),
        "apiKey"
    )

    @Test
    fun `given successful response then emits mapped current weather`() = runTest {
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        val result = service.getCurrentWeather("locationId")

        val expectedCurrentWeather = CurrentWeather(
            currentTemperature = 23.9f,
            weatherStatus = "Sunny",
            weatherIcon = 1,
            isDay = true,
            expirationDate = null
        )
        assertEquals(expectedCurrentWeather, result)
    }

    @Test
    fun `given successful response with expiration date then emits current weather with expiration date`() = runTest {
        val receivedDate = "Thu, 30 Aug 2012 14:56:34 GMT"
        val expectedDate = OffsetDateTime.of(2012, 8, 30, 14, 56, 34, 0, ZoneOffset.UTC)
        val response = getSuccessfulMockResponse().setHeader("Expires", receivedDate)
        mockWebServer.enqueue(response)

        val result = service.getCurrentWeather("locationId")

        assertEquals(expectedDate, result.expirationDate)
    }

    @Test
    fun `when invoke then calls api with correct parameters`() = runTest {
        val locationId = "locationId"
        val response = getSuccessfulMockResponse()
        mockWebServer.enqueue(response)

        service.getCurrentWeather(locationId)

        val request = mockWebServer.takeRequest()
        val requestUrl = request.requestUrl!!
        assertEquals("GET", request.method, "Method")
        assertEquals("apiKey", requestUrl.queryParameter("apikey"), "API Key")
        assertEquals("en-US", requestUrl.queryParameter("language"), "Language")
        assertEquals("/currentconditions/v1/${locationId}", requestUrl.encodedPath, "URL Path")
    }

    @AfterEach
    fun afterEach() {
        mockWebServer.shutdown()
    }

    private fun getSuccessfulMockResponse() = MockResponse()
        .setResponseCode(HttpURLConnection.HTTP_OK)
        .setBody(MockResponseFileReader("current_weather/success.json").content)
}
