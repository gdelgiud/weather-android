package android.util

/**
 * A JVM implementation of [SparseIntArray] that allows running Unit tests that use said class
 * without requiring the use of Robolectric.
 *
 * Its contract and package must match exactly the original one https://developer.android.com/reference/android/util/SparseIntArray
 * Otherwise, tests will fail with [NoSuchMethodException].
 */
class SparseIntArray {

    private val values = HashMap<Int, Int>()

    fun get(key: Int): Int {
        return values.getOrDefault(key, 0)
    }

    fun put(key: Int, value: Int) {
        values[key] = value
    }
}
