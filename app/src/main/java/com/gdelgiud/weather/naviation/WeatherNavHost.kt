package com.gdelgiud.weather.naviation

import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.gdelgiud.weather.dashboard.ui.DashboardRoute
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.search.ui.SearchRoute

@Composable
fun WeatherNavHost(selectedLocationStorage: SelectedLocationStorage, navController: NavHostController) {
    val startDestination = if (selectedLocationStorage.isPresent()) "dashboard" else "search"
    NavHost(navController = navController, startDestination = startDestination) {
        composable(
            "search?modal={modal}",
            arguments = listOf(navArgument("modal") { defaultValue = false }),
            enterTransition = { EnterTransition.None },
            exitTransition = { ExitTransition.None }
        ) { backstackEntry ->
            SearchRoute(
                isModal = backstackEntry.arguments?.getBoolean("modal") ?: false,
                navController = navController
            )
        }
        composable("dashboard") {
            DashboardRoute(navController = navController)
        }
    }
}
