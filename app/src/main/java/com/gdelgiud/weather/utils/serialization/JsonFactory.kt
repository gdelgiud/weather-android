package com.gdelgiud.weather.utils.serialization

import kotlinx.serialization.json.Json

class JsonFactory {

    fun getJson() = Json {
        ignoreUnknownKeys = true
    }
}
