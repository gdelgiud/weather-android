package com.gdelgiud.weather.utils.networking

import com.gdelgiud.weather.utils.serialization.JsonFactory
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import okhttp3.HttpUrl
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit

class ApiServiceFactory(baseUrl: HttpUrl, jsonFactory: JsonFactory) {

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(jsonFactory.getJson().asConverterFactory("application/json".toMediaType()))
        .build()

    fun <T> create(type: Class<T>): T = retrofit.create(type)
}
