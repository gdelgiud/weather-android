package com.gdelgiud.weather.utils.networking

import okhttp3.Headers
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

fun Headers.getOffsetDateTime(headerName: String): OffsetDateTime? {
    val headerValue = get(headerName)
    return if (headerValue != null) {
        OffsetDateTime.parse(get(headerName), DateTimeFormatter.RFC_1123_DATE_TIME)
    } else {
        null
    }
}
