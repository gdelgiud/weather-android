package com.gdelgiud.weather.utils.time

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface TimeModule {

    @Binds
    fun provideTimeProvider(timeProvider: DefaultTimeProvider): TimeProvider
}
