package com.gdelgiud.weather.utils.serialization

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.text.ParseException
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

object OffsetDateTimeSerializer : KSerializer<OffsetDateTime> {

    private val DATE_FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor("DateSerializer", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): OffsetDateTime {
        val dateAsString = decoder.decodeString()
        return try {
            OffsetDateTime.parse(dateAsString, DATE_FORMATTER)
                ?: throw IllegalArgumentException(getErrorMessage(dateAsString))
        } catch (e: ParseException) {
            throw IllegalArgumentException(getErrorMessage(dateAsString), e)
        }
    }

    override fun serialize(encoder: Encoder, value: OffsetDateTime) {
        encoder.encodeString(DATE_FORMATTER.format(value))
    }

    private fun getErrorMessage(dateAsString: String) = "Couldn't parse date: $dateAsString"
}
