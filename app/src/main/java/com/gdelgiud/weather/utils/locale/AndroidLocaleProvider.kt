package com.gdelgiud.weather.utils.locale

import android.content.res.Resources
import android.os.Build
import androidx.core.os.ConfigurationCompat
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AndroidLocaleProvider @Inject constructor(): LocaleProvider {

    override fun getLanguageTag(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getFirstLocale().toLanguageTag()
        } else {
            getFirstLocale().language
        }
    }

    override fun getLocale(): Locale = getFirstLocale()

    private fun getFirstLocale(): Locale =
        ConfigurationCompat.getLocales(Resources.getSystem().configuration)[0]!!

}
