package com.gdelgiud.weather.utils.time

import dagger.Reusable
import java.time.OffsetDateTime
import javax.inject.Inject

@Reusable
class DefaultTimeProvider @Inject constructor() : TimeProvider {

    override fun now(): OffsetDateTime = OffsetDateTime.now()

}
