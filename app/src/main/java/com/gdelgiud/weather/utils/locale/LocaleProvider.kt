package com.gdelgiud.weather.utils.locale

import java.util.Locale

interface LocaleProvider {

    fun getLanguageTag(): String

    fun getLocale(): Locale

}
