package com.gdelgiud.weather.utils.time

import java.time.OffsetDateTime

interface TimeProvider {

    fun now(): OffsetDateTime

}
