package com.gdelgiud.weather.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.gdelgiud.weather.ui.theme.Color.BlueGrey50
import com.gdelgiud.weather.ui.theme.Color.Indigo500
import com.gdelgiud.weather.ui.theme.Color.Indigo700
import com.gdelgiud.weather.ui.theme.Color.LightBlue500
import com.gdelgiud.weather.ui.theme.Color.LightBlue700

private val LightColorPalette = lightColors(
    primary = Indigo500,
    primaryVariant = Indigo700,
    secondary = LightBlue500,
    secondaryVariant = LightBlue700,
    surface = BlueGrey50,
    background = BlueGrey50,
    onPrimary = Color.White,
    onBackground = Color.Black,
    onSecondary = Color.White,
    onSurface = Color.Black
)

@Composable
fun WeatherTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colors = LightColorPalette,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
