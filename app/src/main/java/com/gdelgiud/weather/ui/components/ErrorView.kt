package com.gdelgiud.weather.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gdelgiud.weather.R
import com.gdelgiud.weather.ui.theme.WeatherTheme

@Composable
fun ErrorView(textColor: Color = Color.Unspecified) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.generic_error_title),
            style = MaterialTheme.typography.h5.copy(color = textColor)
        )
        Spacer(modifier = Modifier.size(8.dp))
        Text(
            text = stringResource(R.string.generic_error_description),
            style = MaterialTheme.typography.subtitle1.copy(color = textColor)
        )
    }
}

@Preview
@Composable
fun ErrorViewPreview() {
    WeatherTheme {
        Surface {
            ErrorView()
        }
    }
}
