package com.gdelgiud.weather.ui.theme

import androidx.compose.ui.graphics.Color

@Suppress("MagicNumber")
object Color {
    val Indigo100 = Color(0xFFC5CAE9)
    val Indigo500 = Color(0xFF3F51B5)
    val Indigo700 = Color(0xFF303F9F)

    val LightBlue500 = Color(0xFF03A9F4)
    val LightBlue700 = Color(0xFF0288D1)

    val Blue200 = Color(0xFF90C9F9)
    val Blue500 = Color(0xFF2196F3)
    val Blue700 = Color(0xFF1976D2)

    val BlueGrey50 = Color(0xFFECEFF1)
    val BlueGrey800 = Color(0xFF37474F)
    val BlueGrey900 = Color(0xFF263238)

}

