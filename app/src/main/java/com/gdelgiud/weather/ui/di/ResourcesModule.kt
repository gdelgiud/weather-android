package com.gdelgiud.weather.ui.di

import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class ResourcesModule {

    @Provides
    fun provideResource(@ApplicationContext context: Context): Resources = context.resources
}
