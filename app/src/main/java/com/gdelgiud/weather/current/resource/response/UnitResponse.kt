package com.gdelgiud.weather.current.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class UnitResponse(
    val Value: Float
)
