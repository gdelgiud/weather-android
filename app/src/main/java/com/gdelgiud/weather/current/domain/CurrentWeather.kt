package com.gdelgiud.weather.current.domain

import java.time.OffsetDateTime

data class CurrentWeather(
    val currentTemperature: Float,
    val weatherStatus: String,
    val weatherIcon: Int, // TODO Probably should be an enum
    val isDay: Boolean,
    val expirationDate: OffsetDateTime? = null
) {

    fun isExpired(now: OffsetDateTime) = expirationDate?.isBefore(now) ?: true
}
