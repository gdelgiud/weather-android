package com.gdelgiud.weather.current.resource

import com.gdelgiud.weather.current.resource.response.CurrentWeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AccuWeatherCurrentWeatherApi {

    @GET("currentconditions/v1/{locationId}?details=true")
    suspend fun getCurrentWeather(
        @Path("locationId") locationId: String,
        @Query("apikey") apiKey: String,
        @Query("language") language: String
    ): Response<List<CurrentWeatherResponse>>
}
