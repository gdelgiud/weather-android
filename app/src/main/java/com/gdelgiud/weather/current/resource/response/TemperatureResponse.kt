package com.gdelgiud.weather.current.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class TemperatureResponse(
    val Metric: UnitResponse
)
