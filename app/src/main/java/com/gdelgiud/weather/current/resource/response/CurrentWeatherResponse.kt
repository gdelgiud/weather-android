package com.gdelgiud.weather.current.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class CurrentWeatherResponse(
    val WeatherText: String,
    val Temperature: TemperatureResponse,
    val WeatherIcon: Int,
    val IsDayTime: Boolean
)
