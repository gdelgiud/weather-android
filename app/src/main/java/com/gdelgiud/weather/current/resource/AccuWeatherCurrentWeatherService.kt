package com.gdelgiud.weather.current.resource

import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.current.domain.CurrentWeatherService
import com.gdelgiud.weather.current.resource.response.CurrentWeatherResponse
import com.gdelgiud.weather.di.ApiKey
import com.gdelgiud.weather.utils.locale.LocaleProvider
import com.gdelgiud.weather.utils.networking.getOffsetDateTime
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccuWeatherCurrentWeatherService @Inject constructor(
    private val currentWeatherApi: AccuWeatherCurrentWeatherApi,
    private val localeProvider: LocaleProvider,
    @ApiKey private val apiKey: String
) : CurrentWeatherService {

    override suspend fun getCurrentWeather(locationId: String): CurrentWeather {
        val response = currentWeatherApi.getCurrentWeather(
            locationId,
            apiKey,
            localeProvider.getLanguageTag()
        )
        return response.toCurrentWeather()
    }

    private fun Response<List<CurrentWeatherResponse>>.toCurrentWeather(): CurrentWeather {
        val currentWeatherResponse = body()!![0]
        return CurrentWeather(
            currentTemperature = currentWeatherResponse.Temperature.Metric.Value,
            weatherStatus = currentWeatherResponse.WeatherText,
            weatherIcon = currentWeatherResponse.WeatherIcon,
            isDay = currentWeatherResponse.IsDayTime,
            expirationDate = headers().getOffsetDateTime("Expires")
        )
    }

}
