package com.gdelgiud.weather.current.domain

interface CurrentWeatherService {
    suspend fun getCurrentWeather(locationId: String): CurrentWeather
}
