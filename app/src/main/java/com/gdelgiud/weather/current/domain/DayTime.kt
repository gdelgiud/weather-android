package com.gdelgiud.weather.current.domain

enum class DayTime {
    DAY,
    NIGHT
}
