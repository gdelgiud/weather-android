package com.gdelgiud.weather.search.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class LocationSearchResponse(
    val LocalizedName: String,
    val Key: String,
    val Country: CountryResponse)
