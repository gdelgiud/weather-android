package com.gdelgiud.weather.search.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gdelgiud.weather.R
import com.gdelgiud.weather.search.domain.LocationHistoryStorage
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.search.domain.SearchLocationService
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.search.ui.model.Error
import com.gdelgiud.weather.search.ui.model.Idle
import com.gdelgiud.weather.search.ui.model.Loading
import com.gdelgiud.weather.search.ui.model.LocationResultUiModel
import com.gdelgiud.weather.search.ui.model.LocationSearchUiState
import com.gdelgiud.weather.search.ui.model.Results
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchLocationService: SearchLocationService,
    private val selectedLocationStorage: SelectedLocationStorage,
    private val locationHistoryStorage: LocationHistoryStorage
) : ViewModel() {

    private val _uiState = MutableStateFlow<LocationSearchUiState>(Idle(""))
    val uiState: StateFlow<LocationSearchUiState> get() = _uiState
    private val searchChannel: MutableStateFlow<String> = MutableStateFlow("")

    private var latestSearchResults: List<LocationResult> = emptyList()
        set(value) {
            field = value
            onSearchResultsChanged(value)
        }
    private var latestSearchTerm: String = ""
        set(value) {
            field = value
            onSearchTermChanged(value)
        }

    init {
        viewModelScope.launch {
            searchChannel
                .debounce(SEARCH_DELAY_TIME_MILLIS)
                .filter { searchText -> searchText.isNotBlank() }
                .map { term -> searchLocationService.search(term) }
                .catch { throwable -> onSearchError(throwable) }
                .collect { results -> onSearchResults(results) }
        }
        onSearchTermChanged(latestSearchTerm)
    }

    private fun onSearchTermChanged(newSearchTerm: String) {
        if (newSearchTerm.isNotBlank()) {
            if (latestSearchResults.isEmpty()) {
                _uiState.value = Loading(newSearchTerm)
            } else {
                _uiState.value = Results(
                    results = latestSearchResults.map(this::toUiModel),
                    searchText = latestSearchTerm
                )
            }
        } else {
            viewModelScope.launch {
                latestSearchResults = locationHistoryStorage.getHistory()
                    .sortedByDescending(LocationResult::creationTime)
                    .take(MAX_RECENT_ITEMS_COUNT)
            }

        }
        viewModelScope.launch { searchChannel.emit(newSearchTerm) }
    }

    private fun onSearchResultsChanged(value: List<LocationResult>) {
        if (value.isNotEmpty()) {
            _uiState.value = Results(results = value.map(this::toUiModel), searchText = latestSearchTerm)
        } else {
            _uiState.value = Idle(latestSearchTerm)
        }
    }

    fun onResultItemClicked(selectedLocationId: String) {
        val locationToStore = latestSearchResults.find { it.id == selectedLocationId }
        if (locationToStore != null) {
            viewModelScope.launch {
                selectedLocationStorage.store(locationToStore)
                locationHistoryStorage.store(locationToStore)
            }
        }
    }

    private fun toUiModel(result: LocationResult): LocationResultUiModel {
        return LocationResultUiModel(
            result.id,
            result.name,
            result.countryName,
            getResultIcon(result)
        )
    }

    private fun getResultIcon(result: LocationResult) =
        when (result.source) {
            LocationResult.Source.HISTORY -> R.drawable.ic_history
            LocationResult.Source.RESULT -> R.drawable.ic_place
        }

    private fun onSearchResults(results: List<LocationResult>) {
        latestSearchResults = results
    }

    private fun onSearchError(error: Throwable) {
        Timber.e(error, "Search error")
        _uiState.value = Error(latestSearchTerm)
    }

    fun onSearchTextChanged(searchText: String) {
        latestSearchTerm = searchText
    }

    companion object {
        const val SEARCH_DELAY_TIME_MILLIS = 300L
        const val MAX_RECENT_ITEMS_COUNT = 10
    }

}
