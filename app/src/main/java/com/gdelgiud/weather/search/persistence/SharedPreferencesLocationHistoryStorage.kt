package com.gdelgiud.weather.search.persistence

import android.content.SharedPreferences
import com.gdelgiud.weather.search.di.LocationHistory
import com.gdelgiud.weather.search.domain.LocationHistoryStorage
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.utils.time.TimeProvider
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesLocationHistoryStorage @Inject constructor(
    @LocationHistory private val sharedPreferences: SharedPreferences,
    private val timeProvider: TimeProvider,
    private val json: Json
) : LocationHistoryStorage {

    override suspend fun getHistory(): List<LocationResult> =
        sharedPreferences.all.entries
            .mapNotNull { entry -> entry.value }
            .map { rawValue -> decode(rawValue) }
            .map { persistence -> toLocation(persistence) }

    private fun decode(rawValue: Any) =
        json.decodeFromString<SelectedLocationPersistence>(rawValue.toString())

    private fun toLocation(persistence: SelectedLocationPersistence) =
        LocationResult(
            persistence.locationId,
            persistence.cityName,
            persistence.countryName,
            persistence.creationDate,
            LocationResult.Source.HISTORY
        )

    override suspend fun store(item: LocationResult) {
        val persistence = SelectedLocationPersistence(
            item.id,
            item.name,
            item.countryName,
            timeProvider.now()
        )
        val raw = json.encodeToString(persistence)
        sharedPreferences.edit().putString(item.id, raw).apply()
    }

}
