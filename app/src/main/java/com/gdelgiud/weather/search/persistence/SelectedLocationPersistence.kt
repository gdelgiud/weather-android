@file:UseSerializers(OffsetDateTimeSerializer::class)

package com.gdelgiud.weather.search.persistence


import com.gdelgiud.weather.utils.serialization.OffsetDateTimeSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.time.OffsetDateTime

@Serializable
data class SelectedLocationPersistence(
    val locationId: String,
    val cityName: String,
    val countryName: String,
    val creationDate: OffsetDateTime
)
