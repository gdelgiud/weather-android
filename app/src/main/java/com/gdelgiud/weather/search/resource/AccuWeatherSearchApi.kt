package com.gdelgiud.weather.search.resource

import com.gdelgiud.weather.search.resource.response.LocationSearchResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface AccuWeatherSearchApi {

    @GET("locations/v1/cities/autocomplete")
    suspend fun search(
        @Query("q") term: String,
        @Query("apikey") apiKey: String,
        @Query("language") language: String
    ): List<LocationSearchResponse>
}
