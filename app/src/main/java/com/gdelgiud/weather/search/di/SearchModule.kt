package com.gdelgiud.weather.search.di

import android.content.Context
import android.content.SharedPreferences
import com.gdelgiud.weather.search.domain.LocationHistoryStorage
import com.gdelgiud.weather.search.domain.SearchLocationService
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.search.persistence.SharedPreferencesLocationHistoryStorage
import com.gdelgiud.weather.search.persistence.SharedPreferencesSelectedLocationStorage
import com.gdelgiud.weather.search.resource.AccuWeatherSearchLocationService
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface SearchModule {

    @Binds
    fun provideSearchLocationService(service: AccuWeatherSearchLocationService): SearchLocationService

    @Binds
    fun provideLocationHistoryStorage(storage: SharedPreferencesLocationHistoryStorage):
            LocationHistoryStorage

    @Binds
    fun provideSelectedLocationStorage(storage: SharedPreferencesSelectedLocationStorage):
            SelectedLocationStorage

    companion object {

        private const val LOCATION_HISTORY_SHARED_PREFERENCES_NAME = "LOCATION_HISTORY"
        private const val SELECTED_LOCATION_SHARED_PREFERENCES_NAME = "SELECTED_LOCATION"

        @Provides
        @LocationHistory
        fun provideLocationHistorySharedPreferences(@ApplicationContext context: Context):
                SharedPreferences =
            context.getSharedPreferences(
                LOCATION_HISTORY_SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE
            )

        @Provides
        @SelectedLocation
        fun provideSelectedLocationSharedPreferences(@ApplicationContext context: Context):
                SharedPreferences =
            context.getSharedPreferences(
                SELECTED_LOCATION_SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE
            )

    }

}
