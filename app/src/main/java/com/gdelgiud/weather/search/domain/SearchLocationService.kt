package com.gdelgiud.weather.search.domain

interface SearchLocationService {
    suspend fun search(term: String): List<LocationResult>
}
