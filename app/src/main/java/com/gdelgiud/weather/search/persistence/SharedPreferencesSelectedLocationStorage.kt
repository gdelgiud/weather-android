package com.gdelgiud.weather.search.persistence

import android.content.SharedPreferences
import com.gdelgiud.weather.search.di.SelectedLocation
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.utils.time.TimeProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesSelectedLocationStorage @Inject constructor(
    @SelectedLocation private val sharedPreferences: SharedPreferences,
    private val timeProvider: TimeProvider
) : SelectedLocationStorage {

    private val state = MutableStateFlow(getLatest())

    override suspend fun store(selectedLocation: LocationResult) {
        val persistence = SelectedLocationPersistence(
            selectedLocation.id,
            selectedLocation.name,
            selectedLocation.countryName,
            timeProvider.now()
        )
        val raw = Json.encodeToString(persistence)
        sharedPreferences
            .edit()
            .putString(SELECTED_LOCATION_KEY, raw)
            .apply()
        state.emit(getLatest())
    }

    override fun get(): Flow<LocationResult> {
        return state.filterNotNull()
    }

    override fun getLatest(): LocationResult? {
        val raw = sharedPreferences.getString(SELECTED_LOCATION_KEY, null)
        return if (raw != null) {
            val persistence = Json.decodeFromString<SelectedLocationPersistence>(raw)
            LocationResult(
                persistence.locationId,
                persistence.cityName,
                persistence.countryName,
                persistence.creationDate,
                LocationResult.Source.RESULT
            )
        } else {
            null
        }
    }

    override fun isPresent(): Boolean {
        return sharedPreferences.contains(SELECTED_LOCATION_KEY)
    }

    companion object {
        private const val SELECTED_LOCATION_KEY = "SELECTED_LOCATION"
    }

}
