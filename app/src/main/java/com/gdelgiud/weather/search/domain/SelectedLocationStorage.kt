package com.gdelgiud.weather.search.domain

import kotlinx.coroutines.flow.Flow

interface SelectedLocationStorage {

    suspend fun store(selectedLocation: LocationResult)

    fun get(): Flow<LocationResult>

    fun getLatest(): LocationResult?

    fun isPresent(): Boolean
}
