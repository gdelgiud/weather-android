package com.gdelgiud.weather.search.domain

interface LocationHistoryStorage {

    suspend fun getHistory(): List<LocationResult>

    suspend fun store(item: LocationResult)
}
