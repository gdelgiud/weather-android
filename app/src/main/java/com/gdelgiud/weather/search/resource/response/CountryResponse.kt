package com.gdelgiud.weather.search.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class CountryResponse(
    val LocalizedName: String
)
