package com.gdelgiud.weather.search.ui.model

sealed class LocationSearchUiState {
    abstract val searchText: String
}

data class Results(
    val results: List<LocationResultUiModel>,
    override val searchText: String,
) : LocationSearchUiState()

data class Error(
    override val searchText: String
) : LocationSearchUiState()

data class Loading(
    override val searchText: String
) : LocationSearchUiState()

data class Idle(
    override val searchText: String
) : LocationSearchUiState()

