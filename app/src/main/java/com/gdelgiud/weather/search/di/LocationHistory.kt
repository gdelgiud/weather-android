package com.gdelgiud.weather.search.di

import javax.inject.Qualifier

@Qualifier
annotation class LocationHistory
