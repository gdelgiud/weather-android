package com.gdelgiud.weather.search.ui

import androidx.annotation.DrawableRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ContentAlpha
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.gdelgiud.weather.R
import com.gdelgiud.weather.search.ui.model.Error
import com.gdelgiud.weather.search.ui.model.Idle
import com.gdelgiud.weather.search.ui.model.Loading
import com.gdelgiud.weather.search.ui.model.LocationResultUiModel
import com.gdelgiud.weather.search.ui.model.LocationSearchUiState
import com.gdelgiud.weather.search.ui.model.Results
import com.gdelgiud.weather.ui.components.ErrorView
import com.gdelgiud.weather.ui.theme.WeatherTheme

@Composable
fun SearchRoute(isModal: Boolean, navController: NavController) {
    val viewModel = hiltViewModel<SearchViewModel>()
    val state = viewModel.uiState.collectAsState().value
    SearchScreen(
        state = state,
        isModal = isModal,
        onCloseButtonClick = navController::popBackStack,
        onSearchTextChange = viewModel::onSearchTextChanged,
        onSearchResultClick = { selectedLocationId ->
            viewModel.onResultItemClicked(selectedLocationId)
            navController.navigate("dashboard")
        }
    )
}

@Composable
fun SearchScreen(
    state: LocationSearchUiState,
    isModal: Boolean,
    onCloseButtonClick: () -> Unit = {},
    onSearchTextChange: (String) -> Unit = {},
    onSearchResultClick: (String) -> Unit = {}
) {
    WeatherTheme {
        Surface(
            color = MaterialTheme.colors.primary,
            contentColor = MaterialTheme.colors.onPrimary,
            modifier = Modifier.fillMaxSize()
        ) {
            Column(
                Modifier
                    .systemBarsPadding()
                    .padding(top = 8.dp)
                    .imePadding()
            ) {
                TopBar(
                    searchText = state.searchText,
                    isModal = isModal,
                    onCloseButtonClick = onCloseButtonClick,
                    onSearchTextChange = onSearchTextChange
                )
                when (state) {
                    is Results -> SearchResults(results = state.results, onSearchResultClick = onSearchResultClick)
                    is Error -> SearchErrorView()
                    is Loading -> SearchFullScreenLoading()
                    is Idle -> { /* No-op */ }
                }
            }
        }
    }
}

@Composable
fun TopBar(
    searchText: String,
    isModal: Boolean,
    onCloseButtonClick: () -> Unit,
    onSearchTextChange: (String) -> Unit
) {
    Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.padding(horizontal = 12.dp)) {
        if (isModal) {
            CloseButton(onCloseButtonClick)
            Spacer(modifier = Modifier.size(12.dp))
        }
        SearchTextField(searchText = searchText, onTextChanged = onSearchTextChange)
    }
}

@Composable
fun CloseButton(onClick: () -> Unit) {
    Icon(
        painter = painterResource(R.drawable.ic_close),
        contentDescription = stringResource(R.string.close),
        modifier = Modifier
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(bounded = false),
                onClick = onClick
            )
            .size(32.dp)
            .padding(4.dp)
    )
}

@Composable
fun SearchTextField(searchText: String, onTextChanged: (String) -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current
    TextField(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(bounded = true),
                onClick = { /* No-op*/ }
            ),
        value = searchText,
        maxLines = 1,
        placeholder = { SearchTextPlaceholder() },
        textStyle = MaterialTheme.typography.subtitle1,
        leadingIcon = { Icon(Icons.Default.Search, contentDescription = null) },
        colors = TextFieldDefaults.textFieldColors(
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            placeholderColor = LocalContentColor.current,
            leadingIconColor = LocalContentColor.current,
            cursorColor = MaterialTheme.colors.secondary
        ),
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
        keyboardActions = KeyboardActions(onSearch = { keyboardController?.hide() }),
        shape = MaterialTheme.shapes.large,
        onValueChange = onTextChanged,
    )
}

@Composable
fun SearchTextPlaceholder() {
    Text(
        text = stringResource(R.string.search_edit_text_hint),
        style = MaterialTheme.typography.subtitle1,
        modifier = Modifier.alpha(ContentAlpha.medium)
    )
}

@Composable
fun SearchResults(results: List<LocationResultUiModel>, onSearchResultClick: (String) -> Unit) {
    LazyColumn(contentPadding = PaddingValues(top = 16.dp)) {
        items(results) { result ->
            SearchResultItem(
                locationId = result.id,
                cityName = result.cityName,
                countryName = result.countryName,
                iconResource = result.iconResource,
                onClick = onSearchResultClick
            )
        }
    }
}

@Composable
fun SearchResultItem(
    locationId: String,
    cityName: String,
    countryName: String,
    @DrawableRes iconResource: Int,
    onClick: (String) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick(locationId) }
            .padding(horizontal = 16.dp, vertical = 8.dp)
    ) {
        Icon(
            modifier = Modifier.size(24.dp),
            painter = painterResource(id = iconResource),
            contentDescription = null
        )
        Spacer(modifier = Modifier.size(16.dp))
        Column {
            Text(
                style = MaterialTheme.typography.body1,
                text = cityName
            )
            Text(
                style = MaterialTheme.typography.body2,
                text = countryName,
                modifier = Modifier.alpha(ContentAlpha.medium)
            )
        }
    }
}

@Composable
fun SearchErrorView() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        ErrorView()
    }
}

@Composable
fun SearchFullScreenLoading() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CircularProgressIndicator(color = MaterialTheme.colors.secondary)
    }
}

@Preview
@Composable
fun TopBarPreview() {
    TopBar(
        searchText = "Buenos Aires",
        isModal = true,
        onCloseButtonClick = {},
        onSearchTextChange = {}
    )
}

@Preview
@Composable
fun SearchResultItemPreview() {
    SearchResultItem(
        locationId = "id",
        cityName = "Buenos Aires",
        countryName = "Argentina",
        iconResource = R.drawable.ic_place,
        onClick = {}
    )
}

@Preview(showSystemUi = true)
@Composable
fun SearchFullScreenLoadingPreview() {
    SearchScreen(state = Loading("A city"), isModal = true)
}

@Preview(showSystemUi = true)
@Composable
fun SearchErrorPreview() {
    SearchScreen(state = Error("A city"), isModal = true)
}

@Suppress("MagicNumber")
@Preview(showSystemUi = true)
@Composable
fun SearchResultsModal() {
    SearchScreen(
        state = Results(
            results = generateSequence { LocationResultUiModel(
                id = "id",
                cityName = "Buenos Aires",
                countryName = "Argentina",
                iconResource = R.drawable.ic_history
            ) }.take(n = 24).toList(),
            searchText = "A city"
        ), isModal = true
    )
}
