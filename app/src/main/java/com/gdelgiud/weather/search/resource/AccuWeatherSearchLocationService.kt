package com.gdelgiud.weather.search.resource

import com.gdelgiud.weather.di.ApiKey
import com.gdelgiud.weather.search.domain.LocationResult
import com.gdelgiud.weather.search.domain.SearchLocationService
import com.gdelgiud.weather.search.resource.response.LocationSearchResponse
import com.gdelgiud.weather.utils.locale.LocaleProvider
import com.gdelgiud.weather.utils.time.TimeProvider
import javax.inject.Inject

class AccuWeatherSearchLocationService @Inject constructor(
    private val searchApi: AccuWeatherSearchApi,
    private val localeProvider: LocaleProvider,
    private val timeProvider: TimeProvider,
    @ApiKey private val apiKey: String
) : SearchLocationService {

    override suspend fun search(term: String): List<LocationResult> {
        return searchApi.search(term, apiKey, localeProvider.getLanguageTag())
            .map { toLocationResultList(it) }
    }

    private fun toLocationResultList(response: LocationSearchResponse) =
        LocationResult(
            id = response.Key,
            name = response.LocalizedName,
            countryName = response.Country.LocalizedName,
            creationTime = timeProvider.now(),
            source = LocationResult.Source.RESULT
        )

}
