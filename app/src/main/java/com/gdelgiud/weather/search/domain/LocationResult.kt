package com.gdelgiud.weather.search.domain

import java.time.OffsetDateTime

data class LocationResult(
    val id: String,
    val name: String,
    val countryName: String,
    val creationTime: OffsetDateTime,
    val source: Source,
) {
    enum class Source {
        HISTORY,
        RESULT
    }
}
