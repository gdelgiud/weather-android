package com.gdelgiud.weather.search.ui.model

import androidx.annotation.DrawableRes

data class LocationResultUiModel(
    val id: String,
    val cityName: String,
    val countryName: String,
    @DrawableRes val iconResource: Int
)
