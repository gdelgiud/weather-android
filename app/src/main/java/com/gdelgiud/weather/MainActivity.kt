package com.gdelgiud.weather

import android.graphics.Color
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.navigation.compose.rememberNavController
import com.gdelgiud.weather.naviation.WeatherNavHost
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.ui.theme.WeatherTheme
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject lateinit var selectedLocationStorage: SelectedLocationStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge(
            statusBarStyle = SystemBarStyle.dark(Color.TRANSPARENT),
            navigationBarStyle = SystemBarStyle.dark(Color.TRANSPARENT)
        )
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            WeatherTheme {
                WeatherNavHost(selectedLocationStorage, navController = navController)
            }
        }
    }

}
