package com.gdelgiud.weather.forecast.daily.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class DailyForecastResponse(
    val DailyForecasts: List<DailyForecastItemResponse>,
    val Headline: DailyForecastHeadlineResponse
)
