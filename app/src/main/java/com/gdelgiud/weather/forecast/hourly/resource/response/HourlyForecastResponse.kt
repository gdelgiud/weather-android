@file:UseSerializers(OffsetDateTimeSerializer::class)

package com.gdelgiud.weather.forecast.hourly.resource.response

import com.gdelgiud.weather.current.resource.response.UnitResponse
import com.gdelgiud.weather.utils.serialization.OffsetDateTimeSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.time.OffsetDateTime

@Serializable
data class HourlyForecastResponse(
    val DateTime: OffsetDateTime,
    val WeatherIcon: Int,
    val PrecipitationProbability: Float,
    val Temperature: UnitResponse,
    val IconPhrase: String
)
