package com.gdelgiud.weather.forecast.hourly.resource

import com.gdelgiud.weather.forecast.hourly.resource.response.HourlyForecastResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AccuWeatherHourlyForecastApi {

    @GET("forecasts/v1/hourly/12hour/{locationId}?metric=true")
    suspend fun get12HourForecast(
        @Path("locationId") locationId: String,
        @Query("apikey") apiKey: String,
        @Query("language") language: String
    ): Response<List<HourlyForecastResponse>>
}
