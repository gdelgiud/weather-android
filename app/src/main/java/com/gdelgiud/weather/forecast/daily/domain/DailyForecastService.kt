package com.gdelgiud.weather.forecast.daily.domain

interface DailyForecastService {

    suspend fun get5DayForecast(locationId: String): DailyForecast
}
