package com.gdelgiud.weather.forecast.daily.domain

import java.time.OffsetDateTime

data class DayForecast(
    val date: OffsetDateTime,
    val minimumTemperature: Float,
    val maximumTemperature: Float,
    val schedule: Schedule,
    val weatherIcon: Int // TODO Probably should be an enum
)
