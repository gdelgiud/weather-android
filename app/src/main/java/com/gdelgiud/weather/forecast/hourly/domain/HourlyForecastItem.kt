package com.gdelgiud.weather.forecast.hourly.domain

import java.time.OffsetDateTime

data class HourlyForecastItem(
    val date: OffsetDateTime,
    val weatherStatus: String,
    val weatherIcon: Int, // TODO Probably should be an enum
    val precipitationProbability: Float,
    val temperature: Float
)
