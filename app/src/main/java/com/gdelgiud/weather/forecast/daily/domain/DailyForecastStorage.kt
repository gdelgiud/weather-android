package com.gdelgiud.weather.forecast.daily.domain

interface DailyForecastStorage {

    suspend fun get(locationId: String): DailyForecast?

    suspend fun store(locationId: String, forecast: DailyForecast)

    suspend fun remove(locationId: String)

}
