package com.gdelgiud.weather.forecast.daily.di

import android.content.Context
import android.content.SharedPreferences
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastStorage
import com.gdelgiud.weather.forecast.daily.persistence.SharedPreferencesDailyForecastStorage
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DailyForecastModule {

    @Binds
    fun provideDailyForecastStorage(storage: SharedPreferencesDailyForecastStorage): DailyForecastStorage

    companion object {
        private const val DAILY_FORECAST_CACHE = "daily_forecast_cache"

        @Provides
        @DailyForecastCache
        fun provideDailyForecastSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
            context.getSharedPreferences(DAILY_FORECAST_CACHE, Context.MODE_PRIVATE)

    }
}
