package com.gdelgiud.weather.forecast.daily.domain

import java.time.OffsetDateTime

data class DailyForecast(
    val days: List<DayForecast>,
    val description: String,
    val expirationDate: OffsetDateTime? = null
)
