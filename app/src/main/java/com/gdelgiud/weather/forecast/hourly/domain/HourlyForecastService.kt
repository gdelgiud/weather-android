package com.gdelgiud.weather.forecast.hourly.domain

interface HourlyForecastService {

    suspend fun get12HoursForecast(locationId: String): HourlyForecast
}
