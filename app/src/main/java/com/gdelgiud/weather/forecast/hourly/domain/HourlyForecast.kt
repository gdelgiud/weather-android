package com.gdelgiud.weather.forecast.hourly.domain

import java.time.OffsetDateTime

data class HourlyForecast(
    val hours: List<HourlyForecastItem>,
    val expirationDate: OffsetDateTime? = null
)
