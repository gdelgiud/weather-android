package com.gdelgiud.weather.forecast.daily.domain

import java.time.OffsetDateTime

data class Schedule(
    val sunrise: OffsetDateTime,
    val sunset: OffsetDateTime
)
