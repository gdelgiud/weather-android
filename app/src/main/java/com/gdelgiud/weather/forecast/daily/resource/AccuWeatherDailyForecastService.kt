package com.gdelgiud.weather.forecast.daily.resource

import com.gdelgiud.weather.di.ApiKey
import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DayForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastService
import com.gdelgiud.weather.forecast.daily.domain.Schedule
import com.gdelgiud.weather.forecast.daily.resource.response.DailyForecastItemResponse
import com.gdelgiud.weather.forecast.daily.resource.response.DailyForecastResponse
import com.gdelgiud.weather.utils.locale.LocaleProvider
import com.gdelgiud.weather.utils.networking.getOffsetDateTime
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccuWeatherDailyForecastService @Inject constructor(
    private val dailyForecastApi: AccuWeatherDailyForecastApi,
    private val localeProvider: LocaleProvider,
    @ApiKey private val apiKey: String
) : DailyForecastService {

    override suspend fun get5DayForecast(locationId: String): DailyForecast {
        val response = dailyForecastApi.get5DayForecast(
            locationId,
            apiKey,
            localeProvider.getLanguageTag()
        )
        return response.toDailyForecastList()
    }

    private fun Response<DailyForecastResponse>.toDailyForecastList(): DailyForecast {
        val body = body()!!
        return DailyForecast(
            days = body.DailyForecasts.map { it.toDailyForecastItem() },
            description = body.Headline.Text,
            expirationDate = headers().getOffsetDateTime("Expires")
        )
    }

    private fun DailyForecastItemResponse.toDailyForecastItem(): DayForecast {
        return DayForecast(
            date = Date,
            minimumTemperature = Temperature.Minimum.Value,
            maximumTemperature = Temperature.Maximum.Value,
            schedule = toSchedule(),
            weatherIcon = Day.Icon
        )
    }

    private fun DailyForecastItemResponse.toSchedule() =
        Schedule(
            sunrise = Sun.Rise,
            sunset = Sun.Set
        )

}
