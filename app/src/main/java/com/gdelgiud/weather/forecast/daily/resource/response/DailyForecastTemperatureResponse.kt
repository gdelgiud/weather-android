package com.gdelgiud.weather.forecast.daily.resource.response

import com.gdelgiud.weather.current.resource.response.UnitResponse
import kotlinx.serialization.Serializable

@Serializable
data class DailyForecastTemperatureResponse(
    val Minimum: UnitResponse,
    val Maximum: UnitResponse
)
