package com.gdelgiud.weather.forecast.daily.resource

import com.gdelgiud.weather.forecast.daily.resource.response.DailyForecastResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AccuWeatherDailyForecastApi {

    @GET("forecasts/v1/daily/5day/{locationId}?metric=true&details=true")
    suspend fun get5DayForecast(
        @Path("locationId") locationId: String,
        @Query("apikey") apiKey: String,
        @Query("language") language: String
    ): Response<DailyForecastResponse>
}
