@file:UseSerializers(OffsetDateTimeSerializer::class)

package com.gdelgiud.weather.forecast.daily.persistence

import com.gdelgiud.weather.utils.serialization.OffsetDateTimeSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.time.OffsetDateTime

@Serializable
data class DailyForecastPersistence(
    val days: List<DayPersistence>,
    val description: String,
    val expiration_date: OffsetDateTime
) {

    @Serializable
    data class DayPersistence(
        val date: OffsetDateTime,
        val minimum_temperature: Float,
        val maximum_temperature: Float,
        val weather_icon: Int,
        val sunrise: OffsetDateTime,
        val sunset: OffsetDateTime
    )
}
