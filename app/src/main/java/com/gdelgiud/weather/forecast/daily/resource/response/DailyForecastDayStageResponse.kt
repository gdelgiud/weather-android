package com.gdelgiud.weather.forecast.daily.resource.response

import kotlinx.serialization.Serializable

@Serializable
data class DailyForecastDayStageResponse(
    val Icon: Int
)
