package com.gdelgiud.weather.forecast.hourly.resource

import com.gdelgiud.weather.di.ApiKey
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastItem
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastService
import com.gdelgiud.weather.forecast.hourly.resource.response.HourlyForecastResponse
import com.gdelgiud.weather.utils.locale.LocaleProvider
import com.gdelgiud.weather.utils.networking.getOffsetDateTime
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccuWeatherHourlyForecastService @Inject constructor(
    private val hourlyForecastApi: AccuWeatherHourlyForecastApi,
    private val localeProvider: LocaleProvider,
    @ApiKey private val apiKey: String
) : HourlyForecastService {


    override suspend fun get12HoursForecast(locationId: String): HourlyForecast {
        val response =
            hourlyForecastApi.get12HourForecast(locationId, apiKey, localeProvider.getLanguageTag())
        return response.toHourlyForecastList()
    }

    private fun Response<List<HourlyForecastResponse>>.toHourlyForecastList(): HourlyForecast {
        return HourlyForecast(
            hours = body()!!.map { it.toHourlyForecastItem() },
            expirationDate = headers().getOffsetDateTime("Expires")
        )
    }

    private fun HourlyForecastResponse.toHourlyForecastItem(): HourlyForecastItem {
        return HourlyForecastItem(
            date = DateTime,
            weatherStatus = IconPhrase,
            weatherIcon = WeatherIcon,
            precipitationProbability = PrecipitationProbability,
            temperature = Temperature.Value)
    }

}
