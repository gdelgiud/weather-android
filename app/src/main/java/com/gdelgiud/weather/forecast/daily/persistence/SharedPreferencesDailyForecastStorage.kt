package com.gdelgiud.weather.forecast.daily.persistence

import android.content.SharedPreferences
import com.gdelgiud.weather.forecast.daily.di.DailyForecastCache
import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DayForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastStorage
import com.gdelgiud.weather.forecast.daily.domain.Schedule
import dagger.Reusable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

@Reusable
class SharedPreferencesDailyForecastStorage @Inject constructor(
    @DailyForecastCache private val sharedPreferences: SharedPreferences,
    private val json: Json
) : DailyForecastStorage {

    override suspend fun get(locationId: String): DailyForecast? {
        val raw = sharedPreferences.getString(locationId, null)
        return if (raw != null) {
            val persistence = json.decodeFromString<DailyForecastPersistence>(raw)
            persistence.toDailyForecast()
        } else {
            null
        }
    }

    private fun DailyForecastPersistence.toDailyForecast() =
        DailyForecast(
            days = days.map { it.toDay() },
            description = description,
            expirationDate = expiration_date
        )

    private fun DailyForecastPersistence.DayPersistence.toDay() =
        DayForecast(
            date = date,
            minimumTemperature = minimum_temperature,
            maximumTemperature = maximum_temperature,
            schedule = toSchedule(),
            weatherIcon = weather_icon
        )

    private fun DailyForecastPersistence.DayPersistence.toSchedule() =
        Schedule(
            sunrise = sunrise,
            sunset = sunset
        )

    override suspend fun store(locationId: String, forecast: DailyForecast) {
        if (forecast.expirationDate == null) {
            return
        }
        val raw = json.encodeToString(forecast.toPersistence())
        sharedPreferences.edit().putString(locationId, raw).apply()
    }

    private fun DailyForecast.toPersistence() =
        DailyForecastPersistence(
            days = days.map { it.toDayPersistence() },
            description = description,
            expiration_date = expirationDate!!
        )

    private fun DayForecast.toDayPersistence() =
        DailyForecastPersistence.DayPersistence(
            date = date,
            minimum_temperature = minimumTemperature,
            maximum_temperature = maximumTemperature,
            weather_icon = weatherIcon,
            sunrise = schedule.sunrise,
            sunset = schedule.sunset
        )

    override suspend fun remove(locationId: String) {
        sharedPreferences.edit().remove(locationId).apply()
    }

}
