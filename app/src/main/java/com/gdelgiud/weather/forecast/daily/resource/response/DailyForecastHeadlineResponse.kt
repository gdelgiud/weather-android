package com.gdelgiud.weather.forecast.daily.resource.response

import kotlinx.serialization.Serializable

@Serializable
class DailyForecastHeadlineResponse(
    val Text: String
)
