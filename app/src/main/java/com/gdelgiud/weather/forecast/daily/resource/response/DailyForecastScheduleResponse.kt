@file:UseSerializers(OffsetDateTimeSerializer::class)

package com.gdelgiud.weather.forecast.daily.resource.response

import com.gdelgiud.weather.utils.serialization.OffsetDateTimeSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.time.OffsetDateTime

@Serializable
data class DailyForecastScheduleResponse(
    val Rise: OffsetDateTime,
    val Set: OffsetDateTime
)
