package com.gdelgiud.weather.forecast.daily.di

import javax.inject.Qualifier

@Qualifier
annotation class DailyForecastCache
