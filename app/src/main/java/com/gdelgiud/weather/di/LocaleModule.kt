package com.gdelgiud.weather.di

import com.gdelgiud.weather.utils.locale.AndroidLocaleProvider
import com.gdelgiud.weather.utils.locale.LocaleProvider
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface LocaleModule {

    @Binds
    fun provideLanguageProvider(provider: AndroidLocaleProvider): LocaleProvider

}
