package com.gdelgiud.weather.di

import com.gdelgiud.weather.utils.networking.ApiServiceFactory
import com.gdelgiud.weather.utils.serialization.JsonFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.HttpUrl.Companion.toHttpUrl
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkingModule {

    @Provides
    @Singleton
    fun provideApiServiceFactory(): ApiServiceFactory =
        ApiServiceFactory("https://dataservice.accuweather.com/".toHttpUrl(), JsonFactory())
}
