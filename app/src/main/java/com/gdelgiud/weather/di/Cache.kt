package com.gdelgiud.weather.di

import javax.inject.Qualifier

@Qualifier
annotation class Cache
