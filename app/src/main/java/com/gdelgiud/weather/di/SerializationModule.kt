package com.gdelgiud.weather.di

import com.gdelgiud.weather.utils.serialization.JsonFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json

@InstallIn(SingletonComponent::class)
@Module
class SerializationModule {

    @Provides
    fun provideJson(): Json {
        return JsonFactory().getJson()
    }

}
