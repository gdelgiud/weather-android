package com.gdelgiud.weather.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object StorageModule {

    private const val CACHE_SHARED_PREFERENCES_NAME = "cache_shared_preferences"

    @Provides
    @Cache
    fun provideCacheSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        context.getSharedPreferences(CACHE_SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
}
