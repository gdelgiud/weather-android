package com.gdelgiud.weather.dashboard.ui.model

import com.gdelgiud.weather.current.domain.DayTime

sealed interface DashboardUiState {
    val cityName: String
    val countryName: String
    val dayTime: DayTime?
}

data class Success(
    override val cityName: String,
    override val countryName: String,
    override val dayTime: DayTime?,
    val currentWeather: CurrentWeatherUiModel,
    val hourlyForecast: List<HourlyForecastUiModel>,
    val dailyForecast: List<DailyForecastUiModel>,
    val dailyForecastDescription: String,
) : DashboardUiState

data class Loading(
    override val cityName: String,
    override val countryName: String,
    override val dayTime: DayTime?,
) : DashboardUiState

data class Error(
    override val cityName: String,
    override val countryName: String,
    override val dayTime: DayTime?,
): DashboardUiState
