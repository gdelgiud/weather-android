package com.gdelgiud.weather.dashboard.ui.model

import androidx.annotation.DrawableRes

data class HourlyForecastUiModel(
    val temperature: String,
    val hourText: String,
    val weatherStatus: String,
    val precipitationProbabilityText: String,
    @DrawableRes val weatherIconRes: Int
)
