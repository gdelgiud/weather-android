package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.current.domain.DayTime
import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastStorage
import com.gdelgiud.weather.forecast.daily.domain.DayForecast
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.utils.time.TimeProvider
import dagger.Reusable
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import java.time.OffsetDateTime
import javax.inject.Inject

/**
 * A class that predicts whether the selected location is currently in day time or night time, based
 * on the cached daily forecast. It used to show the proper background color in the Dashboard while
 * the Current Weather request is done.
 */
@Reusable
class DayTimePredictor @Inject constructor(
    private val dailyForecastStorage: DailyForecastStorage,
    private val selectedLocationStorage: SelectedLocationStorage,
    private val timeProvider: TimeProvider
) {
    fun getDayTime(): DayTime? {
        val now = timeProvider.now()
        val todaysForecast = getTodaysForecast(now) ?: return null
        return todaysForecast.getDayTime(now)
    }

    private fun getTodaysForecast(date: OffsetDateTime): DayForecast? {
        val forecast = getForecast() ?: return null
        return forecast.getForecastForDate(date)
    }

    private fun DailyForecast.getForecastForDate(now: OffsetDateTime): DayForecast? {
        return days.firstOrNull { it.date.toLocalDate().isEqual(now.toLocalDate()) }
    }

    private fun getForecast(): DailyForecast? {
        return runBlocking {
            if (!selectedLocationStorage.isPresent()) {
                return@runBlocking null
            }
            val selectedLocation = selectedLocationStorage.get().first()
            val forecastResult = runCatching { dailyForecastStorage.get(selectedLocation.id) }
            return@runBlocking forecastResult.getOrElse { t ->
                Timber.e(t)
                dailyForecastStorage.remove(selectedLocation.id)
                null
            }
        }
    }

    private fun DayForecast.getDayTime(now: OffsetDateTime): DayTime {
        return if (isDuringDayTime(now)) {
            DayTime.DAY
        } else {
            DayTime.NIGHT
        }
    }

    private fun DayForecast.isDuringDayTime(time: OffsetDateTime) =
        time.isAfter(schedule.sunrise) && time.isBefore(schedule.sunset)
}
