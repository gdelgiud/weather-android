package com.gdelgiud.weather.dashboard.ui.model

import androidx.annotation.DrawableRes

data class CurrentWeatherUiModel(
    val currentTemperature: String,
    val weatherStatus: String,
    @DrawableRes val weatherIconRes: Int
)
