package com.gdelgiud.weather.dashboard.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gdelgiud.weather.dashboard.domain.DayTimePredictor
import com.gdelgiud.weather.dashboard.domain.LoadDashboardService
import com.gdelgiud.weather.dashboard.ui.model.DashboardUiModel
import com.gdelgiud.weather.dashboard.ui.model.DashboardUiState
import com.gdelgiud.weather.dashboard.ui.model.Error
import com.gdelgiud.weather.dashboard.ui.model.Loading
import com.gdelgiud.weather.dashboard.ui.model.Success
import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val selectedLocationStorage: SelectedLocationStorage,
    private val loadDashboardService: LoadDashboardService,
    private val dashboardInfoToUiModelMapper: DashboardInfoToUiModelMapper,
    private val dayTimePredictor: DayTimePredictor
) : ViewModel() {

    private val _dashboardData = MutableStateFlow(getLoadingState())
    val dashboardData: StateFlow<DashboardUiState> = _dashboardData

    private var hasError = false

    fun onStart() {
        loadDashboard()
    }

    private fun loadDashboard() = loadDashboardService.loadedDashboard
        .map { dashboardInfoToUiModelMapper.map(it) }
        .onEach { onLoadSuccess(it) }
        .catch { throwable -> onLoadError(throwable) }
        .launchIn(viewModelScope)

    fun onRetryClicked() {
        if (hasError) {
            loadDashboard()
        }
    }

    private fun getLoadingState(): DashboardUiState {
        val selectedLocation =
            selectedLocationStorage.getLatest() ?: throw IllegalStateException("No selected location")
        return Loading(
            selectedLocation.name,
            selectedLocation.countryName,
            dayTime = dayTimePredictor.getDayTime()
        )
    }

    private fun onLoadSuccess(model: DashboardUiModel) {
        _dashboardData.value = Success(
            cityName = model.cityName,
            countryName = model.countryName,
            dayTime = model.dayTime,
            currentWeather = model.currentWeather,
            hourlyForecast = model.hourlyForecast,
            dailyForecastDescription = model.dailyForecastDescription,
            dailyForecast = model.dailyForecast
        )
        hasError = false
    }

    private fun onLoadError(error: Throwable) {
        Timber.e(error, "Weather fetch error")
        hasError = true
        _dashboardData.value = Error(
            cityName = _dashboardData.value.cityName,
            countryName = _dashboardData.value.countryName,
            dayTime = _dashboardData.value.dayTime
        )
    }

}
