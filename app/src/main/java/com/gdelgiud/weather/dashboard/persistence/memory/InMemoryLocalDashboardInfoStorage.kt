package com.gdelgiud.weather.dashboard.persistence.memory

import com.gdelgiud.weather.dashboard.domain.DashboardInfo
import com.gdelgiud.weather.dashboard.domain.LocalDashboardInfoStorage
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InMemoryLocalDashboardInfoStorage @Inject constructor() : LocalDashboardInfoStorage {

    private val state = MutableStateFlow(State(mutableMapOf()))

    override suspend fun get(locationId: String): Flow<DashboardInfo?> {
        return state.map { it.state[locationId] }
    }

    override suspend fun store(locationId: String, dashboard: DashboardInfo) {
        state.value.state[locationId] = dashboard
        state.emit(State(state.value.state))
    }

    override suspend fun remove(locationId: String) {
        state.value.state.remove(locationId)
        state.emit(State(state.value.state))
    }

    /**
     * A workaround used to ensure values are always emitted from a StateFlow. A StateFlow always
     * includes the operator `distinctUntilChanged`, which prevents from emitting the same map repeatedly.
     *
     * This class without [equals] makes sure a value is always emitted.
     */
    private inner class State(val state: MutableMap<String, DashboardInfo>)
}
