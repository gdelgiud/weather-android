package com.gdelgiud.weather.dashboard.domain

import kotlinx.coroutines.flow.Flow

interface LocalDashboardInfoStorage {

    suspend fun get(locationId: String): Flow<DashboardInfo?>

    suspend fun store(locationId: String, dashboard: DashboardInfo)

    suspend fun remove(locationId: String)
}
