package com.gdelgiud.weather.dashboard.ui.model

import androidx.annotation.DrawableRes

data class DailyForecastUiModel(
    val dayName: String,
    val minimumTemperature: String,
    val maximumTemperature: String,
    @DrawableRes val weatherIconRes: Int
)
