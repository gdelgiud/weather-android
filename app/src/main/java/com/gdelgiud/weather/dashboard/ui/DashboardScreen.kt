package com.gdelgiud.weather.dashboard.ui

import androidx.annotation.DrawableRes
import androidx.compose.animation.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.DeviceFontFamilyName
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.compose.LifecycleEventEffect
import androidx.navigation.NavController
import com.gdelgiud.weather.R
import com.gdelgiud.weather.current.domain.DayTime
import com.gdelgiud.weather.dashboard.ui.model.CurrentWeatherUiModel
import com.gdelgiud.weather.dashboard.ui.model.DailyForecastUiModel
import com.gdelgiud.weather.dashboard.ui.model.DashboardUiState
import com.gdelgiud.weather.dashboard.ui.model.Error
import com.gdelgiud.weather.dashboard.ui.model.HourlyForecastUiModel
import com.gdelgiud.weather.dashboard.ui.model.Loading
import com.gdelgiud.weather.dashboard.ui.model.Success
import com.gdelgiud.weather.ui.components.ErrorView
import com.gdelgiud.weather.ui.theme.WeatherTheme

private const val BACKGROUND_TRANSITION_DURATION = 2000

@Composable
fun DashboardRoute(navController: NavController) {
    val viewModel = hiltViewModel<DashboardViewModel>()
    LifecycleEventEffect(Lifecycle.Event.ON_START) {
        viewModel.onStart()
    }
    val state = viewModel.dashboardData.collectAsState().value
    DashboardScreen(state = state, onLocationIndicatorClick = { navController.navigate("search?modal=true") })
}

@Composable
fun DashboardScreen(state: DashboardUiState, onLocationIndicatorClick: () -> Unit) {
    WeatherTheme {
        val defaultColor = MaterialTheme.colors.secondary
        val savedStartColor = rememberSaveable { mutableIntStateOf(defaultColor.toArgb()) }
        val savedEndColor = rememberSaveable { mutableIntStateOf(defaultColor.toArgb()) }
        val startColor = remember { Animatable(Color(savedStartColor.intValue)) }
        val endColor = remember { Animatable(Color(savedEndColor.intValue)) }
        val targetStartColor: Color
        val targetEndColor: Color
        when (state.dayTime) {
            DayTime.DAY -> {
                targetStartColor = colorResource(R.color.daytime_background_gradient_start)
                targetEndColor = colorResource(R.color.daytime_background_gradient_end)
            }
            DayTime.NIGHT -> {
                targetStartColor = colorResource(R.color.nighttime_background_gradient_start)
                targetEndColor = colorResource(R.color.nighttime_background_gradient_end)
            }
            null -> {
                targetStartColor = MaterialTheme.colors.secondary
                targetEndColor = MaterialTheme.colors.secondary
            }
        }
        val backgroundTransitionDuration = if (state is Success) BACKGROUND_TRANSITION_DURATION else 0
        LaunchedEffect(targetStartColor, targetEndColor, backgroundTransitionDuration) {
            startColor.animateTo(targetStartColor, animationSpec = tween(backgroundTransitionDuration))
            savedStartColor.intValue = targetStartColor.toArgb()
        }
        LaunchedEffect(targetStartColor, targetEndColor, backgroundTransitionDuration) {
            endColor.animateTo(targetEndColor, animationSpec = tween(backgroundTransitionDuration))
            savedEndColor.intValue = targetEndColor.toArgb()
        }
        Surface(
            color = Color.Transparent,
            contentColor = MaterialTheme.colors.onPrimary,
            modifier = Modifier.background(
                Brush.verticalGradient(listOf(startColor.value, endColor.value))
            )
        ) {
            Column(modifier = Modifier.systemBarsPadding()) {
                LocationIndicator(
                    cityName = state.cityName,
                    countryName = state.countryName,
                    onClick = onLocationIndicatorClick
                )
                when (state) {
                    is Error -> DashboardErrorView()
                    is Loading -> LoadingScreen()
                    is Success -> DashboardContent(state)
                }
            }
        }
    }
}

@Composable
fun LoadingScreen() {
    DashboardFullScreenLoading()
}

@Composable
fun DashboardContent(state: Success) {
    Column {
        Box(contentAlignment = Alignment.Center, modifier = Modifier
            .weight(1f, true)
            .fillMaxWidth()) {
            CurrentWeather(
                weatherStatus = state.currentWeather.weatherStatus,
                temperature = state.currentWeather.currentTemperature,
                weatherIcon = state.currentWeather.weatherIconRes
            )
        }
        HourlyForecast(forecastItems = state.hourlyForecast)
        Spacer(modifier = Modifier.size(8.dp))
        DailyForecast(description = state.dailyForecastDescription, forecastItems = state.dailyForecast)
    }
}

@Composable
fun LocationIndicator(cityName: String, countryName: String, onClick: () -> Unit) {
    Column(modifier = Modifier
        .padding(8.dp)
        .clip(MaterialTheme.shapes.medium)
        .clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = rememberRipple(),
            onClick = onClick
        )
        .padding(horizontal = 16.dp, vertical = 4.dp)
    ) {
        Text(
            text = cityName,
            style = MaterialTheme.typography.h5
        )
        Text(
            text = countryName,
            style = MaterialTheme.typography.subtitle1
        )
    }
}

@Composable
fun CurrentWeather(weatherStatus: String, temperature: String, @DrawableRes weatherIcon: Int) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.Center,
    ) {
        Text(
            text = weatherStatus,
            style = MaterialTheme.typography.h4,
            maxLines = 2
        )
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Image(
                painter = painterResource(weatherIcon),
                contentDescription = null,
                modifier = Modifier.size(48.dp)
            )
            Text(
                text = temperature,
                style = MaterialTheme.typography.h3
            )
        }
    }
}

@Composable
fun HourlyForecast(forecastItems: List<HourlyForecastUiModel>) {
    Column {
        Text(
            text = stringResource(R.string.dashboard_hourly_forecast_label),
            style = MaterialTheme.typography.h6,
            modifier = Modifier.padding(start = 16.dp)
        )
        LazyRow {
            items(forecastItems) { item ->
                HourlyForecastItem(item)
            }
        }
    }
}

@Composable
fun HourlyForecastItem(item: HourlyForecastUiModel) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .width(102.dp)
            .padding(8.dp)
    ) {
        Text(
            text = item.hourText,
            style = MaterialTheme.typography.subtitle2,
            textAlign = TextAlign.Center
        )
        // Ugly hack to center text vertically with minLines set
        Box(contentAlignment = Alignment.Center) {
            Text(
                text = "",
                minLines = 2
            )
            Text(
                text = item.weatherStatus,
                style = MaterialTheme.typography.body2,
                overflow = TextOverflow.Ellipsis,
                maxLines = 2,
                textAlign = TextAlign.Center
            )
        }
        Image(
            painter = painterResource(item.weatherIconRes),
            contentDescription = null,
            modifier = Modifier
                .padding(vertical = 8.dp)
                .size(24.dp)
        )
        Text(
            text = item.temperature,
            style = MaterialTheme.typography.body2,
            fontFamily = FontFamily(Font(DeviceFontFamilyName("sans-serif-condensed"))),
            textAlign = TextAlign.Center
        )
        Text(
            text = item.precipitationProbabilityText,
            style = MaterialTheme.typography.body2,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun DailyForecast(description: String, forecastItems: List<DailyForecastUiModel>) {
    Column {
        Text(
            text = stringResource(R.string.dashboard_daily_forecast_label),
            style = MaterialTheme.typography.h6,
            modifier = Modifier.padding(start = 16.dp)
        )
        Text(
            text = description,
            style = MaterialTheme.typography.subtitle2,
            modifier = Modifier.padding(start = 16.dp)
        )
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier.fillMaxWidth()
        ) {
            forecastItems.forEach {
                DailyForecastItem(it)
            }
        }
    }
}

@Composable
fun DailyForecastItem(item: DailyForecastUiModel) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(16.dp)
    ) {
        Text(
            text = item.dayName,
            style = MaterialTheme.typography.subtitle2
        )
        Image(
            painter = painterResource(item.weatherIconRes),
            contentDescription = null,
            modifier = Modifier
                .padding(8.dp)
                .size(32.dp)
        )
        Text(
            text = item.minimumTemperature,
            style = MaterialTheme.typography.body2,
            fontWeight = FontWeight.SemiBold,
            fontFamily = FontFamily(Font(DeviceFontFamilyName("sans-serif-condensed"))),
            modifier = Modifier.alpha(ContentAlpha.medium)
        )
        Text(
            text = item.maximumTemperature,
            style = MaterialTheme.typography.body2,
            fontWeight = FontWeight.SemiBold,
            fontFamily = FontFamily(Font(DeviceFontFamilyName("sans-serif-condensed")))
        )
    }
}

@Composable
fun DashboardErrorView() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        ErrorView()
    }
}

@Composable
fun DashboardFullScreenLoading() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CircularProgressIndicator(color = MaterialTheme.colors.secondary)
    }
}

@Composable
@Preview
fun LocationIndicatorPreview() {
    LocationIndicator(
        cityName = "Buenos Aires",
        countryName = "Argentina",
        onClick = {}
    )
}

@Composable
@Preview
fun CurrentWeatherPreview() {
    CurrentWeather(
        weatherStatus = "Mostly sunny",
        temperature = "23.9°C",
        weatherIcon = R.drawable.weather_icon_mostly_sunny
    )
}

@Composable
@Preview
fun HourlyForecastPreview() {
    HourlyForecast(forecastItems = listOf(
        previewHourlyForecastItem().copy(hourText = "15:00"),
        previewHourlyForecastItem().copy(hourText = "16:00"),
        previewHourlyForecastItem().copy(hourText = "17:00"),
        previewHourlyForecastItem().copy(hourText = "18:00")
    ))
}

@Composable
@Preview
fun HourlyWeatherItemPreview() {
    HourlyForecastItem(previewHourlyForecastItem())
}

@Composable
@Preview
fun DailyForecastPreview() {
    DailyForecast(description = "Pleasant this weekend", forecastItems = listOf(
        previewDailyForecastItem().copy(dayName = "Wed"),
        previewDailyForecastItem().copy(dayName = "Thu"),
        previewDailyForecastItem().copy(dayName = "Fri"),
        previewDailyForecastItem().copy(dayName = "Sat"),
        previewDailyForecastItem().copy(dayName = "Sun")
    ))
}

@Composable
@Preview
fun DailyForecastItemPreview() {
    DailyForecastItem(previewDailyForecastItem())
}

@Composable
@Preview(showSystemUi = true)
fun DashboardLoadingPreview() {
    DashboardScreen(
        state = Loading(
            cityName = "Buenos Aires",
            countryName = "Argentina",
            dayTime = DayTime.DAY
        ),
        onLocationIndicatorClick = {}
    )
}

@Composable
@Preview(showSystemUi = true)
fun DashboardErrorPreview() {
    DashboardScreen(
        state = Error(
            cityName = "Buenos Aires",
            countryName = "Argentina",
            dayTime = DayTime.DAY
        ),
        onLocationIndicatorClick = {}
    )
}

@Composable
@Preview(showSystemUi = true)
fun DashboardContentPreview() {
    DashboardScreen(
        state = Success(
            cityName = "Buenos Aires",
            countryName = "Argentina",
            dayTime = DayTime.DAY,
            currentWeather = CurrentWeatherUiModel(
                weatherStatus = "Mostly sunny",
                currentTemperature = "23°C",
                weatherIconRes = R.drawable.weather_icon_mostly_sunny
            ),
            hourlyForecast = listOf(
                previewHourlyForecastItem().copy(hourText = "15:00"),
                previewHourlyForecastItem().copy(hourText = "16:00"),
                previewHourlyForecastItem().copy(hourText = "17:00"),
                previewHourlyForecastItem().copy(hourText = "18:00")
            ),
            dailyForecast = listOf(
                previewDailyForecastItem().copy(dayName = "Wed"),
                previewDailyForecastItem().copy(dayName = "Thu"),
                previewDailyForecastItem().copy(dayName = "Fri"),
                previewDailyForecastItem().copy(dayName = "Sat"),
                previewDailyForecastItem().copy(dayName = "Sun")
            ),
            dailyForecastDescription = "Pleasant this weekend"
        ),
        onLocationIndicatorClick = {}
    )
}

private fun previewHourlyForecastItem(): HourlyForecastUiModel {
    return HourlyForecastUiModel(
        temperature = "28.3°C",
        hourText = "15:00",
        weatherIconRes = R.drawable.weather_icon_mostly_sunny,
        weatherStatus = "Mostly sunny",
        precipitationProbabilityText = "Precip. 43%"
    )
}

private fun previewDailyForecastItem(): DailyForecastUiModel {
    return DailyForecastUiModel(
        dayName = "Wed",
        minimumTemperature = "17°C",
        maximumTemperature = "23°C",
        weatherIconRes = R.drawable.weather_icon_mostly_sunny
    )
}
