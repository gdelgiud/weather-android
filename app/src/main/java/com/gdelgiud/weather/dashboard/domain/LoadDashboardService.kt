package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.search.domain.SelectedLocationStorage
import com.gdelgiud.weather.utils.time.TimeProvider
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@Reusable
class LoadDashboardService @Inject constructor(
    private val localStorage: LocalDashboardInfoStorage,
    private val remoteService: RemoteDashboardService,
    private val selectedLocationStorage: SelectedLocationStorage,
    private val timeProvider: TimeProvider
) {

    val loadedDashboard: Flow<DashboardInfo> get() = selectedLocationStorage.get()
        .flatMapLatest { location -> localStorage.get(location.id) }
        .map { remoteSource(it) }
        .filterNotNull()
        .distinctUntilChanged()

    private suspend fun remoteSource(localDashboardInfo: DashboardInfo?): DashboardInfo {
        val selectedLocation = selectedLocationStorage.get().first()
        if (localDashboardInfo != null && !localDashboardInfo.isExpired(timeProvider.now())) {
            return localDashboardInfo
        }
        return remoteService.get(selectedLocation).also { storeResult(it, selectedLocation.id) }
    }

    private suspend fun storeResult(dashboardInfo: DashboardInfo, selectedLocationId: String) {
        localStorage.store(selectedLocationId, dashboardInfo)
    }
}
