package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.current.domain.CurrentWeatherService
import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastService
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastStorage
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastService
import com.gdelgiud.weather.search.domain.LocationResult
import dagger.Reusable
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import java.time.OffsetDateTime
import javax.inject.Inject

@Reusable
class DefaultRemoteDashboardService @Inject constructor(
    private val currentWeatherService: CurrentWeatherService,
    private val hourlyForecastService: HourlyForecastService,
    private val dailyForecastService: DailyForecastService,
    private val dailyForecastStorage: DailyForecastStorage
) : RemoteDashboardService {

    override suspend fun get(selectedLocation: LocationResult): DashboardInfo = coroutineScope {
        val currentWeather = async { getCurrentWeather(selectedLocation) }
        val hourlyForecast = async { getHourlyForecast(selectedLocation) }
        val dailyForecast = async { getDailyForecast(selectedLocation) }
        return@coroutineScope combineDashboardElements(
            currentWeather.await(),
            hourlyForecast.await(),
            dailyForecast.await(),
            selectedLocation
        )
    }

    private suspend fun getCurrentWeather(selectedLocation: LocationResult): CurrentWeather {
        return currentWeatherService.getCurrentWeather(selectedLocation.id)
    }

    private suspend fun getHourlyForecast(selectedLocation: LocationResult) =
        hourlyForecastService.get12HoursForecast(selectedLocation.id)

    private suspend fun getDailyForecast(selectedLocation: LocationResult) =
        dailyForecastService.get5DayForecast(selectedLocation.id)
            .also { forecast -> dailyForecastStorage.store(selectedLocation.id, forecast) }

    private fun combineDashboardElements(
        current: CurrentWeather,
        hourly: HourlyForecast,
        daily: DailyForecast,
        selectedLocation: LocationResult
    ): DashboardInfo {
        return DashboardInfo(
            currentWeather = current,
            hourlyForecast = hourly.hours,
            dailyForecast = daily,
            selectedLocation = selectedLocation,
            expirationDate = getExpirationDate(current, hourly, daily)
        )
    }

    private fun getExpirationDate(
        currentWeather: CurrentWeather,
        hourlyForecast: HourlyForecast,
        dailyForecast: DailyForecast
    ): OffsetDateTime? {
        return listOfNotNull(
            currentWeather.expirationDate,
            hourlyForecast.expirationDate,
            dailyForecast.expirationDate
        ).minOrNull()
    }
}
