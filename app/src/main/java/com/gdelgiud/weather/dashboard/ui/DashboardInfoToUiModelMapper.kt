package com.gdelgiud.weather.dashboard.ui

import android.util.SparseIntArray
import androidx.annotation.DrawableRes
import androidx.core.util.set
import com.gdelgiud.weather.R
import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.current.domain.DayTime.DAY
import com.gdelgiud.weather.current.domain.DayTime.NIGHT
import com.gdelgiud.weather.dashboard.domain.DashboardInfo
import com.gdelgiud.weather.dashboard.ui.model.CurrentWeatherUiModel
import com.gdelgiud.weather.dashboard.ui.model.DailyForecastUiModel
import com.gdelgiud.weather.dashboard.ui.model.DashboardUiModel
import com.gdelgiud.weather.dashboard.ui.model.HourlyForecastUiModel
import com.gdelgiud.weather.forecast.daily.domain.DayForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastItem
import com.gdelgiud.weather.utils.locale.LocaleProvider
import dagger.Reusable
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@Reusable
class DashboardInfoToUiModelMapper @Inject constructor(
    private val stringProvider: DashboardStringProvider,
    private val localeProvider: LocaleProvider
) {

    private val weatherIconMap = SparseIntArray()

    init {
        buildResourceMap()
    }

    @Suppress("MagicNumber")
    private fun buildResourceMap() {
        // Reference: https://developer.accuweather.com/weather-icons
        weatherIconMap[1] = R.drawable.weather_icon_sunny
        weatherIconMap[2] = R.drawable.weather_icon_mostly_sunny
        weatherIconMap[3] = R.drawable.weather_icon_partly_sunny
        weatherIconMap[4] = R.drawable.weather_icon_intermittent_clouds
        weatherIconMap[5] = R.drawable.weather_icon_hazy_sunshine
        weatherIconMap[6] = R.drawable.weather_icon_mostly_cloudy
        weatherIconMap[7] = R.drawable.weather_icon_cloudy
        weatherIconMap[8] = R.drawable.weather_icon_overcast
        weatherIconMap[11] = R.drawable.weather_icon_fog
        weatherIconMap[12] = R.drawable.weather_icon_showers
        weatherIconMap[13] = R.drawable.weather_icon_mostly_cloudy_with_showers
        weatherIconMap[14] = R.drawable.weather_icon_partly_sunny_with_showers
        weatherIconMap[15] = R.drawable.weather_icon_t_storms
        weatherIconMap[16] = R.drawable.weather_icon_mostly_cloudy_with_t_storms
        weatherIconMap[17] = R.drawable.weather_icon_partly_sunny_with_t_storms
        weatherIconMap[18] = R.drawable.weather_icon_rain
        weatherIconMap[19] = R.drawable.weather_icon_flurries
        weatherIconMap[20] = R.drawable.weather_icon_mostly_cloudy_with_flurries
        weatherIconMap[21] = R.drawable.weather_icon_partly_sunny_with_flurries
        weatherIconMap[22] = R.drawable.weather_icon_snow
        weatherIconMap[23] = R.drawable.weather_icon_mostly_cloudy_with_snow
        weatherIconMap[24] = R.drawable.weather_icon_ice
        weatherIconMap[25] = R.drawable.weather_icon_sleet
        weatherIconMap[26] = R.drawable.weather_icon_freezing_rain
        weatherIconMap[29] = R.drawable.weather_icon_rain_and_snow
        weatherIconMap[30] = R.drawable.weather_icon_hot
        weatherIconMap[31] = R.drawable.weather_icon_cold
        weatherIconMap[32] = R.drawable.weather_icon_windy
        weatherIconMap[33] = R.drawable.weather_icon_night_clear
        weatherIconMap[34] = R.drawable.weather_icon_night_mostly_clear
        weatherIconMap[36] = R.drawable.weather_icon_night_partly_cloudy
        weatherIconMap[35] = R.drawable.weather_icon_night_intermittent_clouds
        weatherIconMap[37] = R.drawable.weather_icon_night_hazy
        weatherIconMap[38] = R.drawable.weather_icon_night_mostly_cloudy
        weatherIconMap[39] = R.drawable.weather_icon_night_partly_cloudy_with_showers
        weatherIconMap[40] = R.drawable.weather_icon_night_mostly_cloudy_with_showers
        weatherIconMap[41] = R.drawable.weather_icon_night_partly_cloudy_with_t_storms
        weatherIconMap[42] = R.drawable.weather_icon_night_mostly_cloudy_with_t_storms
        weatherIconMap[43] = R.drawable.weather_icon_night_mostly_cloudy_with_flurries
        weatherIconMap[44] = R.drawable.weather_icon_night_mostly_cloudy_with_snow
    }

    fun map(dashboardInfo: DashboardInfo): DashboardUiModel {
        return DashboardUiModel(
            cityName = dashboardInfo.selectedLocation.name,
            countryName = dashboardInfo.selectedLocation.countryName,
            currentWeather = toCurrentWeatherUiModel(dashboardInfo.currentWeather),
            hourlyForecast = toHourlyForecastUiModel(dashboardInfo.hourlyForecast),
            dailyForecast = toDailyForecastUiModel(dashboardInfo.dailyForecast.days),
            dailyForecastDescription = dashboardInfo.dailyForecast.description,
            dayTime = if (dashboardInfo.currentWeather.isDay) DAY else NIGHT
        )
    }

    private fun toCurrentWeatherUiModel(currentWeather: CurrentWeather): CurrentWeatherUiModel {
        val currentTemperature = toTemperatureString(currentWeather.currentTemperature, 1)
        return CurrentWeatherUiModel(
            currentTemperature,
            currentWeather.weatherStatus,
            getWeatherIconRes(currentWeather.weatherIcon)
        )
    }

    private fun toTemperatureString(temperature: Float, decimalPlaces: Int): String {
        return stringProvider.toTemperatureString(temperature, decimalPlaces)
    }

    @DrawableRes
    private fun getWeatherIconRes(weatherIcon: Int): Int {
        return weatherIconMap[weatherIcon]
    }

    private fun toHourlyForecastUiModel(hourlyForecastItem: List<HourlyForecastItem>): List<HourlyForecastUiModel> {
        val hourFormatter = DateTimeFormatter.ofPattern(HOUR_FORMAT, locale())
        return hourlyForecastItem.map {
            HourlyForecastUiModel(
                toTemperatureString(it.temperature, 1),
                hourFormatter.format(it.date),
                it.weatherStatus,
                stringProvider.toPrecipitationProbabilityString(it.precipitationProbability),
                getWeatherIconRes(it.weatherIcon)
            )
        }
    }

    private fun toDailyForecastUiModel(dayForecast: List<DayForecast>): List<DailyForecastUiModel> {
        val dayFormatter = DateTimeFormatter.ofPattern(DAY_FORMAT, locale())
        return dayForecast.map {
            DailyForecastUiModel(
                dayFormatter.format(it.date).replaceFirstChar { c -> c.uppercase(locale()) },
                toTemperatureString(it.minimumTemperature, 0),
                toTemperatureString(it.maximumTemperature, 0),
                getWeatherIconRes(it.weatherIcon)
            )
        }
    }

    private fun locale() = localeProvider.getLocale()

    companion object {
        private const val DAY_FORMAT = "EEE"
        private const val HOUR_FORMAT = "HH:mm"
    }
}
