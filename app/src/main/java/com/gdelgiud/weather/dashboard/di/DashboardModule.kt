package com.gdelgiud.weather.dashboard.di

import com.gdelgiud.weather.current.domain.CurrentWeatherService
import com.gdelgiud.weather.current.resource.AccuWeatherCurrentWeatherService
import com.gdelgiud.weather.dashboard.domain.DefaultRemoteDashboardService
import com.gdelgiud.weather.dashboard.domain.LocalDashboardInfoStorage
import com.gdelgiud.weather.dashboard.domain.RemoteDashboardService
import com.gdelgiud.weather.dashboard.persistence.memory.InMemoryLocalDashboardInfoStorage
import com.gdelgiud.weather.forecast.daily.domain.DailyForecastService
import com.gdelgiud.weather.forecast.daily.resource.AccuWeatherDailyForecastService
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastService
import com.gdelgiud.weather.forecast.hourly.resource.AccuWeatherHourlyForecastService
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface DashboardModule {

    @Binds
    fun provideRemoteDashboardService(service: DefaultRemoteDashboardService): RemoteDashboardService

    @Binds
    fun provideLocalDashboardInfoStorage(storage: InMemoryLocalDashboardInfoStorage): LocalDashboardInfoStorage

    @Binds
    fun provideCurrentWeatherService(service: AccuWeatherCurrentWeatherService): CurrentWeatherService

    @Binds
    fun provideHourlyForecastService(service: AccuWeatherHourlyForecastService): HourlyForecastService

    @Binds
    fun provideDailyForecastService(service: AccuWeatherDailyForecastService): DailyForecastService

}
