package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.search.domain.LocationResult

interface RemoteDashboardService {

    suspend fun get(selectedLocation: LocationResult): DashboardInfo
}
