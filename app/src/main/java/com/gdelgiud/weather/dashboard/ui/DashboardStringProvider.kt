package com.gdelgiud.weather.dashboard.ui

import android.content.res.Resources
import com.gdelgiud.weather.R
import com.gdelgiud.weather.utils.locale.LocaleProvider
import javax.inject.Inject

class DashboardStringProvider @Inject constructor(
    private val resources: Resources,
    private val localeProvider: LocaleProvider
) {

    fun toTemperatureString(temperature: Float, decimalPlaces: Int): String {
        val format = "%.${decimalPlaces}f"
        val formattedTemperature = String.format(localeProvider.getLocale(), format, temperature)
        return resources.getString(R.string.temperature, formattedTemperature)
    }

    fun toPrecipitationProbabilityString(probability: Float) : String {
        val format = "%.0f"
        val formattedProbability = String.format(localeProvider.getLocale(), format, probability)
        return resources.getString(R.string.precipitation_probability, formattedProbability)
    }
}
