package com.gdelgiud.weather.dashboard.ui.model

import com.gdelgiud.weather.current.domain.DayTime

data class DashboardUiModel(
    val cityName: String,
    val countryName: String,
    val dayTime: DayTime?,
    val currentWeather: CurrentWeatherUiModel,
    val hourlyForecast: List<HourlyForecastUiModel>,
    val dailyForecast: List<DailyForecastUiModel>,
    val dailyForecastDescription: String
)
