package com.gdelgiud.weather.dashboard.domain

import com.gdelgiud.weather.current.domain.CurrentWeather
import com.gdelgiud.weather.forecast.daily.domain.DailyForecast
import com.gdelgiud.weather.forecast.hourly.domain.HourlyForecastItem
import com.gdelgiud.weather.search.domain.LocationResult
import java.time.OffsetDateTime

data class DashboardInfo(
    val currentWeather: CurrentWeather,
    val hourlyForecast: List<HourlyForecastItem>,
    val dailyForecast: DailyForecast,
    val selectedLocation: LocationResult,
    val expirationDate: OffsetDateTime? = null
) {

    fun isExpired(now: OffsetDateTime) = expirationDate?.isBefore(now) ?: true
}
