# Weather App

[![pipeline status](https://gitlab.com/gdelgiud/weather-android/badges/master/pipeline.svg)](https://gitlab.com/gdelgiud/weather-android/-/commits/master)

## Description

A simple Weather Application that uses [AccuWeather's API](https://developer.accuweather.com/apis), written in Kotlin. It provides a search screen to pick a city and a dashboard that displays:
- The current temperature
- The current weather
- The forecast for the next 12 hours
- The forecast for the next 5 days

This Application requires API Level 21+.

## Structure

The project is split into two Flavours: **mock** and **production**.

The **mock** Flavour, suitable for development, replaces all AccuWeather's responses with mocked JSON files as raw resources.

The **production** Flavour, intended for final builds, consumes AccuWeather's actual service. A valid API key is needed to use the application in this flavour.

## Building

**Mock** Flavour requires no special considerations for building. Running `./gradlew assembleMock` should be enough.

**Production** Flavour builds require an AccuWeather API key. The API Key is loaded by the build script from the `ACCUWEATHER_API_KEY` environment variable, or from a property with the same name in the **local.properties** file. Run `./gradlew assembleProduction` to produce Debug and Release builds.

**Release** Builds require a Keystore. The Keystore file should be located at the root folder of the project and named **keystore.jks**.  
The data needed to use the Keystore is read from environment variables or properties from **local.properties** with names as follows:
- `SIGNING_KEY_ALIAS`
- `SIGNING_KEY_PASSWORD`
- `SIGNING_STORE_PASSWORD`

## Screenshots

<img src="screenshots/search_screen.png" width="250" alt="Search screen" />
<img src="screenshots/dashboard_daytime.png" width="250" alt="Day time dashboard" />
<img src="screenshots/dashboard_nighttime.png" width="250" alt="Night time dashboard" />

## Technologies

- Kotlin
- Kotlin Coroutines
- Kotlin Flow
- Kotlin Serialization
- ViewModel
- Retrofit
- Timber
- Detekt
- Hilt

## License

```
Copyright 2024 Gustavo Del Giudice

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
